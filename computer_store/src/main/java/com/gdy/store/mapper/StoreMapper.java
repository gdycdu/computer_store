package com.gdy.store.mapper;

import com.gdy.store.pojo.Store;
import com.gdy.store.pojo.UserExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface StoreMapper {
    int deleteByPrimaryKey(Integer storeId);

    int insert(Store record);

    int insertSelective(Store record);

    Store selectByPrimaryKey(Integer storeId);

    int updateByPrimaryKeySelective(Store record);

    int updateByPrimaryKey(Store record);

    Integer getUsersCount();

    Integer getUsersCount2();

    Integer getUsersCount3(@Param("search") String search);

    List<Store> getAllUsers(@Param("offset") int offset, @Param("size") int size);

    List<Store> getAllUsers2(@Param("offset") int offset, @Param("size") int size);

    List<Store> getAllUsers3(@Param("offset") int offset, @Param("size") int size,@Param("search") String search);

    List<Store> selectByExample(UserExample example);

    List<Store> selectByExample2(UserExample example);

    int shenheStore(int storeId,int shenheCode);

    Store selectByUserId(int userId);

    int updateStoreById(int storeId,String storeName,String storeDesc);

    int stopOpen(int storeId);

    int renewOpen(int storeId);

    Store getByGoodsId(int goodsId);
}
