package com.gdy.store.mapper;

import com.gdy.store.pojo.Orderinfo;
import com.gdy.store.pojo.UserExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface OrderinfoMapper {
    int deleteByPrimaryKey(Long orderId);

    int insert(Orderinfo record);

    int insertSelective(Orderinfo record);

    Orderinfo selectByPrimaryKey(Long orderId);

    int updateByPrimaryKeySelective(Orderinfo record);

    int updateByPrimaryKey(Orderinfo record);

    List<Orderinfo> selectByUserId(int userId);

    Integer getUsersCount(int storeId);

    Integer getUsersCount2(int storeId,String search);

    List<Orderinfo> getAllUsers(@Param("offset") int offset, @Param("size") int size, @Param("storeId") int storeId);

    List<Orderinfo> getAllUsers2(@Param("offset") int offset, @Param("size") int size, @Param("storeId") int storeId,@Param("search") String search);

    List<Orderinfo> selectByExample(UserExample example);
    /*发货*/
    int updateGoodsByDelivery(Long id);
    /*确认收货*/
    int getGoodsByIdReceiving(Long id);
    /*完成*/
    int getGoodsByOK(Long id);

    int getOrderCountByStore(int storeId);

    int getOrderCountWaitDeliver(int storeId);

    int getUserCountByOrder(int storeId);

    List<Orderinfo> getOrderByStoreId(int storeId);


}