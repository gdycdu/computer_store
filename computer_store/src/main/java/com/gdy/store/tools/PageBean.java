package com.gdy.store.tools;

import com.alibaba.fastjson.JSONArray;

import java.util.List;

public class PageBean <T> {
	
	private int totalRecs = 0;  //{ps} 总记录数
	private int page = 0;       //{ps} 当前页页码
	private int limit= 0;
	private List<T> list = null;
	private JSONArray jsArr = null;
	
	public List<T> getList(){
		return list;
	}
	public void setList(List<T> list){
		if( list!=null ){
			this.list = list;
			jsArr = new JSONArray();
			for (T t : list) {
				jsArr.add( t );
			}
		}
	}
	public int getTotalRecs() { return totalRecs; }
	public void setTotalRecs(int totalRecs) {
		this.totalRecs = totalRecs;
	}
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	public JSONArray getJsArr() {
		return jsArr;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
}
