package com.gdy.store.pojo;

public class Collect {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column collect.collect_Id
     *
     * @mbg.generated
     */
    private Integer collectId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column collect.collect_User
     *
     * @mbg.generated
     */
    private Integer collectUser;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column collect.collect_Goods
     *
     * @mbg.generated
     */
    private Integer collectGoods;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column collect.collect_State
     *
     * @mbg.generated
     */
    private Integer collectState;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column collect.collect_Id
     *
     * @return the value of collect.collect_Id
     *
     * @mbg.generated
     */
    public Integer getCollectId() {
        return collectId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column collect.collect_Id
     *
     * @param collectId the value for collect.collect_Id
     *
     * @mbg.generated
     */
    public void setCollectId(Integer collectId) {
        this.collectId = collectId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column collect.collect_User
     *
     * @return the value of collect.collect_User
     *
     * @mbg.generated
     */
    public Integer getCollectUser() {
        return collectUser;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column collect.collect_User
     *
     * @param collectUser the value for collect.collect_User
     *
     * @mbg.generated
     */
    public void setCollectUser(Integer collectUser) {
        this.collectUser = collectUser;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column collect.collect_Goods
     *
     * @return the value of collect.collect_Goods
     *
     * @mbg.generated
     */
    public Integer getCollectGoods() {
        return collectGoods;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column collect.collect_Goods
     *
     * @param collectGoods the value for collect.collect_Goods
     *
     * @mbg.generated
     */
    public void setCollectGoods(Integer collectGoods) {
        this.collectGoods = collectGoods;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column collect.collect_State
     *
     * @return the value of collect.collect_State
     *
     * @mbg.generated
     */
    public Integer getCollectState() {
        return collectState;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column collect.collect_State
     *
     * @param collectState the value for collect.collect_State
     *
     * @mbg.generated
     */
    public void setCollectState(Integer collectState) {
        this.collectState = collectState;
    }
}