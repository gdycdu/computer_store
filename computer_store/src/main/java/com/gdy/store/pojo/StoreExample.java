package com.gdy.store.pojo;

import java.util.ArrayList;
import java.util.List;

public class StoreExample {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table store
     *
     * @mbg.generated
     */
    protected String orderByClause;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table store
     *
     * @mbg.generated
     */
    protected boolean distinct;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table store
     *
     * @mbg.generated
     */
    protected List<Criteria> oredCriteria;

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table store
     *
     * @mbg.generated
     */
    public StoreExample() {
        oredCriteria = new ArrayList<>();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table store
     *
     * @mbg.generated
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table store
     *
     * @mbg.generated
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table store
     *
     * @mbg.generated
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table store
     *
     * @mbg.generated
     */
    public boolean isDistinct() {
        return distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table store
     *
     * @mbg.generated
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table store
     *
     * @mbg.generated
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table store
     *
     * @mbg.generated
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table store
     *
     * @mbg.generated
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table store
     *
     * @mbg.generated
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table store
     *
     * @mbg.generated
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table store
     *
     * @mbg.generated
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andStoreIdIsNull() {
            addCriterion("store_Id is null");
            return (Criteria) this;
        }

        public Criteria andStoreIdIsNotNull() {
            addCriterion("store_Id is not null");
            return (Criteria) this;
        }

        public Criteria andStoreIdEqualTo(Integer value) {
            addCriterion("store_Id =", value, "storeId");
            return (Criteria) this;
        }

        public Criteria andStoreIdNotEqualTo(Integer value) {
            addCriterion("store_Id <>", value, "storeId");
            return (Criteria) this;
        }

        public Criteria andStoreIdGreaterThan(Integer value) {
            addCriterion("store_Id >", value, "storeId");
            return (Criteria) this;
        }

        public Criteria andStoreIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("store_Id >=", value, "storeId");
            return (Criteria) this;
        }

        public Criteria andStoreIdLessThan(Integer value) {
            addCriterion("store_Id <", value, "storeId");
            return (Criteria) this;
        }

        public Criteria andStoreIdLessThanOrEqualTo(Integer value) {
            addCriterion("store_Id <=", value, "storeId");
            return (Criteria) this;
        }

        public Criteria andStoreIdIn(List<Integer> values) {
            addCriterion("store_Id in", values, "storeId");
            return (Criteria) this;
        }

        public Criteria andStoreIdNotIn(List<Integer> values) {
            addCriterion("store_Id not in", values, "storeId");
            return (Criteria) this;
        }

        public Criteria andStoreIdBetween(Integer value1, Integer value2) {
            addCriterion("store_Id between", value1, value2, "storeId");
            return (Criteria) this;
        }

        public Criteria andStoreIdNotBetween(Integer value1, Integer value2) {
            addCriterion("store_Id not between", value1, value2, "storeId");
            return (Criteria) this;
        }

        public Criteria andStoreNameIsNull() {
            addCriterion("store_Name is null");
            return (Criteria) this;
        }

        public Criteria andStoreNameIsNotNull() {
            addCriterion("store_Name is not null");
            return (Criteria) this;
        }

        public Criteria andStoreNameEqualTo(String value) {
            addCriterion("store_Name =", value, "storeName");
            return (Criteria) this;
        }

        public Criteria andStoreNameNotEqualTo(String value) {
            addCriterion("store_Name <>", value, "storeName");
            return (Criteria) this;
        }

        public Criteria andStoreNameGreaterThan(String value) {
            addCriterion("store_Name >", value, "storeName");
            return (Criteria) this;
        }

        public Criteria andStoreNameGreaterThanOrEqualTo(String value) {
            addCriterion("store_Name >=", value, "storeName");
            return (Criteria) this;
        }

        public Criteria andStoreNameLessThan(String value) {
            addCriterion("store_Name <", value, "storeName");
            return (Criteria) this;
        }

        public Criteria andStoreNameLessThanOrEqualTo(String value) {
            addCriterion("store_Name <=", value, "storeName");
            return (Criteria) this;
        }

        public Criteria andStoreNameLike(String value) {
            addCriterion("store_Name like", value, "storeName");
            return (Criteria) this;
        }

        public Criteria andStoreNameNotLike(String value) {
            addCriterion("store_Name not like", value, "storeName");
            return (Criteria) this;
        }

        public Criteria andStoreNameIn(List<String> values) {
            addCriterion("store_Name in", values, "storeName");
            return (Criteria) this;
        }

        public Criteria andStoreNameNotIn(List<String> values) {
            addCriterion("store_Name not in", values, "storeName");
            return (Criteria) this;
        }

        public Criteria andStoreNameBetween(String value1, String value2) {
            addCriterion("store_Name between", value1, value2, "storeName");
            return (Criteria) this;
        }

        public Criteria andStoreNameNotBetween(String value1, String value2) {
            addCriterion("store_Name not between", value1, value2, "storeName");
            return (Criteria) this;
        }

        public Criteria andStoreUserIsNull() {
            addCriterion("store_User is null");
            return (Criteria) this;
        }

        public Criteria andStoreUserIsNotNull() {
            addCriterion("store_User is not null");
            return (Criteria) this;
        }

        public Criteria andStoreUserEqualTo(Integer value) {
            addCriterion("store_User =", value, "storeUser");
            return (Criteria) this;
        }

        public Criteria andStoreUserNotEqualTo(Integer value) {
            addCriterion("store_User <>", value, "storeUser");
            return (Criteria) this;
        }

        public Criteria andStoreUserGreaterThan(Integer value) {
            addCriterion("store_User >", value, "storeUser");
            return (Criteria) this;
        }

        public Criteria andStoreUserGreaterThanOrEqualTo(Integer value) {
            addCriterion("store_User >=", value, "storeUser");
            return (Criteria) this;
        }

        public Criteria andStoreUserLessThan(Integer value) {
            addCriterion("store_User <", value, "storeUser");
            return (Criteria) this;
        }

        public Criteria andStoreUserLessThanOrEqualTo(Integer value) {
            addCriterion("store_User <=", value, "storeUser");
            return (Criteria) this;
        }

        public Criteria andStoreUserIn(List<Integer> values) {
            addCriterion("store_User in", values, "storeUser");
            return (Criteria) this;
        }

        public Criteria andStoreUserNotIn(List<Integer> values) {
            addCriterion("store_User not in", values, "storeUser");
            return (Criteria) this;
        }

        public Criteria andStoreUserBetween(Integer value1, Integer value2) {
            addCriterion("store_User between", value1, value2, "storeUser");
            return (Criteria) this;
        }

        public Criteria andStoreUserNotBetween(Integer value1, Integer value2) {
            addCriterion("store_User not between", value1, value2, "storeUser");
            return (Criteria) this;
        }

        public Criteria andStoreDescIsNull() {
            addCriterion("store_Desc is null");
            return (Criteria) this;
        }

        public Criteria andStoreDescIsNotNull() {
            addCriterion("store_Desc is not null");
            return (Criteria) this;
        }

        public Criteria andStoreDescEqualTo(String value) {
            addCriterion("store_Desc =", value, "storeDesc");
            return (Criteria) this;
        }

        public Criteria andStoreDescNotEqualTo(String value) {
            addCriterion("store_Desc <>", value, "storeDesc");
            return (Criteria) this;
        }

        public Criteria andStoreDescGreaterThan(String value) {
            addCriterion("store_Desc >", value, "storeDesc");
            return (Criteria) this;
        }

        public Criteria andStoreDescGreaterThanOrEqualTo(String value) {
            addCriterion("store_Desc >=", value, "storeDesc");
            return (Criteria) this;
        }

        public Criteria andStoreDescLessThan(String value) {
            addCriterion("store_Desc <", value, "storeDesc");
            return (Criteria) this;
        }

        public Criteria andStoreDescLessThanOrEqualTo(String value) {
            addCriterion("store_Desc <=", value, "storeDesc");
            return (Criteria) this;
        }

        public Criteria andStoreDescLike(String value) {
            addCriterion("store_Desc like", value, "storeDesc");
            return (Criteria) this;
        }

        public Criteria andStoreDescNotLike(String value) {
            addCriterion("store_Desc not like", value, "storeDesc");
            return (Criteria) this;
        }

        public Criteria andStoreDescIn(List<String> values) {
            addCriterion("store_Desc in", values, "storeDesc");
            return (Criteria) this;
        }

        public Criteria andStoreDescNotIn(List<String> values) {
            addCriterion("store_Desc not in", values, "storeDesc");
            return (Criteria) this;
        }

        public Criteria andStoreDescBetween(String value1, String value2) {
            addCriterion("store_Desc between", value1, value2, "storeDesc");
            return (Criteria) this;
        }

        public Criteria andStoreDescNotBetween(String value1, String value2) {
            addCriterion("store_Desc not between", value1, value2, "storeDesc");
            return (Criteria) this;
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table store
     *
     * @mbg.generated do_not_delete_during_merge
     */
    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table store
     *
     * @mbg.generated
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}