package com.gdy.store.pojo;

import java.math.BigDecimal;
import java.util.Date;

public class orderGoods2 {

    private String orderId;

    private Integer orderUser;

    private Date orderDate;

    private BigDecimal orderPrice;

    private Integer orderState;

    private String orderReceiver;

    private String orderPhone;

    private String orderAddress;

    private Integer orderStore;

    private Date createTimeOrder;

    private Date updateTimeOrder;

    private Integer goodsId;

    private String goodsName;

    private Integer goodsNums;

    private Integer goodsBrand;

    private Integer goodsType;

    private BigDecimal goodsPrice;

    private String goodsAttr;

    private String goodsDesc;

    private Integer goodsSales;

    private Integer goodsState;

    private String goodsImg;

    private Integer goodsStore;

    private Date createTimeGoods;

    private Date updateTimeGoods;

    private Integer detailId;

    private Long detailOrder;

    private Integer detailGoods;

    private Double detailPrice;

    private Integer detailNum;

    private Integer typeId;

    private String typeName;

    private Integer typeState;

    private Date createTimeGoodsType;

    private Date updateTimeGoodsType;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getOrderUser() {
        return orderUser;
    }

    public void setOrderUser(Integer orderUser) {
        this.orderUser = orderUser;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public BigDecimal getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(BigDecimal orderPrice) {
        this.orderPrice = orderPrice;
    }

    public Integer getOrderState() {
        return orderState;
    }

    public void setOrderState(Integer orderState) {
        this.orderState = orderState;
    }

    public String getOrderReceiver() {
        return orderReceiver;
    }

    public void setOrderReceiver(String orderReceiver) {
        this.orderReceiver = orderReceiver;
    }

    public String getOrderPhone() {
        return orderPhone;
    }

    public void setOrderPhone(String orderPhone) {
        this.orderPhone = orderPhone;
    }

    public String getOrderAddress() {
        return orderAddress;
    }

    public void setOrderAddress(String orderAddress) {
        this.orderAddress = orderAddress;
    }

    public Integer getOrderStore() {
        return orderStore;
    }

    public void setOrderStore(Integer orderStore) {
        this.orderStore = orderStore;
    }

    public Date getCreateTimeOrder() {
        return createTimeOrder;
    }

    public void setCreateTimeOrder(Date createTimeOrder) {
        this.createTimeOrder = createTimeOrder;
    }

    public Date getUpdateTimeOrder() {
        return updateTimeOrder;
    }

    public void setUpdateTimeOrder(Date updateTimeOrder) {
        this.updateTimeOrder = updateTimeOrder;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public Integer getGoodsNums() {
        return goodsNums;
    }

    public void setGoodsNums(Integer goodsNums) {
        this.goodsNums = goodsNums;
    }

    public Integer getGoodsBrand() {
        return goodsBrand;
    }

    public void setGoodsBrand(Integer goodsBrand) {
        this.goodsBrand = goodsBrand;
    }

    public Integer getGoodsType() {
        return goodsType;
    }

    public void setGoodsType(Integer goodsType) {
        this.goodsType = goodsType;
    }

    public BigDecimal getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(BigDecimal goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public String getGoodsAttr() {
        return goodsAttr;
    }

    public void setGoodsAttr(String goodsAttr) {
        this.goodsAttr = goodsAttr;
    }

    public String getGoodsDesc() {
        return goodsDesc;
    }

    public void setGoodsDesc(String goodsDesc) {
        this.goodsDesc = goodsDesc;
    }

    public Integer getGoodsSales() {
        return goodsSales;
    }

    public void setGoodsSales(Integer goodsSales) {
        this.goodsSales = goodsSales;
    }

    public Integer getGoodsState() {
        return goodsState;
    }

    public void setGoodsState(Integer goodsState) {
        this.goodsState = goodsState;
    }

    public String getGoodsImg() {
        return goodsImg;
    }

    public void setGoodsImg(String goodsImg) {
        this.goodsImg = goodsImg;
    }

    public Integer getGoodsStore() {
        return goodsStore;
    }

    public void setGoodsStore(Integer goodsStore) {
        this.goodsStore = goodsStore;
    }

    public Date getCreateTimeGoods() {
        return createTimeGoods;
    }

    public void setCreateTimeGoods(Date createTimeGoods) {
        this.createTimeGoods = createTimeGoods;
    }

    public Date getUpdateTimeGoods() {
        return updateTimeGoods;
    }

    public void setUpdateTimeGoods(Date updateTimeGoods) {
        this.updateTimeGoods = updateTimeGoods;
    }

    public Integer getDetailId() {
        return detailId;
    }

    public void setDetailId(Integer detailId) {
        this.detailId = detailId;
    }

    public Long getDetailOrder() {
        return detailOrder;
    }

    public void setDetailOrder(Long detailOrder) {
        this.detailOrder = detailOrder;
    }

    public Integer getDetailGoods() {
        return detailGoods;
    }

    public void setDetailGoods(Integer detailGoods) {
        this.detailGoods = detailGoods;
    }

    public Double getDetailPrice() {
        return detailPrice;
    }

    public void setDetailPrice(Double detailPrice) {
        this.detailPrice = detailPrice;
    }

    public Integer getDetailNum() {
        return detailNum;
    }

    public void setDetailNum(Integer detailNum) {
        this.detailNum = detailNum;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Integer getTypeState() {
        return typeState;
    }

    public void setTypeState(Integer typeState) {
        this.typeState = typeState;
    }

    public Date getCreateTimeGoodsType() {
        return createTimeGoodsType;
    }

    public void setCreateTimeGoodsType(Date createTimeGoodsType) {
        this.createTimeGoodsType = createTimeGoodsType;
    }

    public Date getUpdateTimeGoodsType() {
        return updateTimeGoodsType;
    }

    public void setUpdateTimeGoodsType(Date updateTimeGoodsType) {
        this.updateTimeGoodsType = updateTimeGoodsType;
    }
}
