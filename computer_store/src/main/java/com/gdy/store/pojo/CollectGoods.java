package com.gdy.store.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
public class CollectGoods {
    private Integer goodsId;
    private String goodsName;
    private Integer goodsNums;
    private BigDecimal goodsPrice;
    private String goodsAttr;
    private String goodsImg;
    private Integer goodsState;
    private Integer collectId;
    private Integer collectUser;
    private Integer collectState;


}
