package com.gdy.store.pojo;

public class Evaimg {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column evaimg.evaImg_id
     *
     * @mbg.generated
     */
    private Integer evaimgId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column evaimg.evaImg_Name
     *
     * @mbg.generated
     */
    private String evaimgName;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column evaimg.evaluate
     *
     * @mbg.generated
     */
    private Integer evaluate;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column evaimg.evaImg_id
     *
     * @return the value of evaimg.evaImg_id
     *
     * @mbg.generated
     */
    public Integer getEvaimgId() {
        return evaimgId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column evaimg.evaImg_id
     *
     * @param evaimgId the value for evaimg.evaImg_id
     *
     * @mbg.generated
     */
    public void setEvaimgId(Integer evaimgId) {
        this.evaimgId = evaimgId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column evaimg.evaImg_Name
     *
     * @return the value of evaimg.evaImg_Name
     *
     * @mbg.generated
     */
    public String getEvaimgName() {
        return evaimgName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column evaimg.evaImg_Name
     *
     * @param evaimgName the value for evaimg.evaImg_Name
     *
     * @mbg.generated
     */
    public void setEvaimgName(String evaimgName) {
        this.evaimgName = evaimgName == null ? null : evaimgName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column evaimg.evaluate
     *
     * @return the value of evaimg.evaluate
     *
     * @mbg.generated
     */
    public Integer getEvaluate() {
        return evaluate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column evaimg.evaluate
     *
     * @param evaluate the value for evaimg.evaluate
     *
     * @mbg.generated
     */
    public void setEvaluate(Integer evaluate) {
        this.evaluate = evaluate;
    }
}