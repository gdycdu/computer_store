package com.gdy.store.pojo;

import java.math.BigDecimal;

public class CartGoods2 {

    private Integer cartId;

    private Integer cartGoods;

    private Integer cartNum;

    private BigDecimal cartPrice;

    private Integer cartUser;

    private Integer goodsId;

    private String goodsName;

    private String goodsAttr;

    private String goodsImg;

    private int goodsNums;

    private Integer typeId;

    private String typeName;

    private BigDecimal AllcartNum;

    public int getGoodsNums() {
        return goodsNums;
    }

    public void setGoodsNums(int goodsNums) {
        this.goodsNums = goodsNums;
    }

    public String getGoodsAttr() {
        return goodsAttr;
    }

    public void setGoodsAttr(String goodsAttr) {
        this.goodsAttr = goodsAttr;
    }

    public BigDecimal getAllcartNum() {
        return AllcartNum;
    }

    public void setAllcartNum(BigDecimal allcartNum) {
        AllcartNum = allcartNum;
    }

    public Integer getCartId() {
        return cartId;
    }

    public void setCartId(Integer cartId) {
        this.cartId = cartId;
    }

    public Integer getCartGoods() {
        return cartGoods;
    }

    public void setCartGoods(Integer cartGoods) {
        this.cartGoods = cartGoods;
    }

    public Integer getCartNum() {
        return cartNum;
    }

    public void setCartNum(Integer cartNum) {
        this.cartNum = cartNum;
    }

    public BigDecimal getCartPrice() {
        return cartPrice;
    }

    public void setCartPrice(BigDecimal cartPrice) {
        this.cartPrice = cartPrice;
    }

    public Integer getCartUser() {
        return cartUser;
    }

    public void setCartUser(Integer cartUser) {
        this.cartUser = cartUser;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsImg() {
        return goodsImg;
    }

    public void setGoodsImg(String goodsImg) {
        this.goodsImg = goodsImg;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
