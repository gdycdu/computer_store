package com.gdy.store.controller;

import com.gdy.store.pojo.*;
import com.gdy.store.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/address")
public class AddressController {
    @Autowired
    private ProvinceService provinceService;
    @Autowired
    private CityService cityService;
    @Autowired
    private AreaService areaService;
    @Autowired
    private StreetService streetService;
    @Autowired
    private AddressService addressService;
    //获取省
    @RequestMapping("/getAllProv")
    @ResponseBody
    public List<Province> getAllProv(){
        return provinceService.getAllProv();
    }
    //根据省获取市
    @RequestMapping("/getCityByProId")
    @ResponseBody
    public List<City> getCityByProId(int provinceId){
        return cityService.getCityByProId(provinceId);
    }
    //根据市获得区
    @RequestMapping("/getAreaByCityId")
    @ResponseBody
    public List<Area> getAreaByCityId(int cityId){
        return areaService.getAreaByCityId(cityId);
    }
    //根据区获得街道
    @RequestMapping("/getStreetByAreaId")
    @ResponseBody
    public List<Street> getStreetByAreaId(int areaId){
        return streetService.getStreetByAreaId(areaId);
    }
    //根据城市获得街道
    @RequestMapping("/getStreetByCityId")
    @ResponseBody
    public List<Street> getStreetByCityId(int cityId){
        return streetService.getStreetByCityId(cityId);
    }
    @RequestMapping("/addAddress")
    @ResponseBody
    public String addAddress(@RequestParam(value = "phone",required = true)String phone,
                             @RequestParam(value = "name",required = true)String name,
                             @RequestParam(value = "province",required = true)String province,
                             @RequestParam(value = "city",required = true)String city,
                             @RequestParam(value = "district",required = true)String district,
                             @RequestParam(value = "jiedao",required = true)String jiedao,
                             @RequestParam(value = "detailsAddress",required = true)String detailsAddress,
                             @RequestParam(value = "youzheng",required = true)String youzheng, HttpSession session){

        int userId  = (int) session.getAttribute("userId");

        Address address = new Address();
        address.setAddrUser(userId);
        address.setAddrPhone(phone);
        address.setAddrReceiver(name);
        address.setAddrProvince(province);
        address.setAddrCity(city);
        address.setAddrArea(district);
        address.setAddrStreet(jiedao);
        address.setAddrDetail(detailsAddress);
        address.setAddrPostcode(youzheng);
        address.setAddrDefault("否");
        address.setAddrState(1);
        address.setCreateTime(new Date());
        address.setUpdateTime(new Date());

        int i = addressService.addAddress(address);
        if(i == 1){
            return "success";
        }else {
            return "flag";
        }
    }

}
