package com.gdy.store.controller;

import com.alibaba.fastjson.JSONObject;
import com.gdy.store.pojo.*;
import com.gdy.store.service.*;
import com.gdy.store.tools.PageBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/store")
public class StoreController {

    @Autowired
    private StoreService storeService;

    @Autowired
    private UserService userService;

    @Autowired
    private GoodsService goodsService;

    @Autowired
    private CollectService collectService;

    @Autowired
    private OrderDetailService orderDetailService;

    @Autowired
    private OrderService orderService;

    @RequestMapping("addStoreBecome")
    @ResponseBody
    public String addStoreBecome(Store store) {
        int rs = storeService.addStoreBecome(store);
        if (rs > 0) {
            return "success";
        } else {
            return "fail";
        }
    }


    /*查询用户全部信息 分页*/
    @RequestMapping("/getAllstore")
    @ResponseBody
    public JSONObject getAllUser(int page, int limit, String keyWord) {
        PageBean pageBean = new PageBean();
        pageBean.setPage(page);
        pageBean.setLimit(limit);
        PageBean<StoreUser> pBean = storeService.getPageBean(pageBean.getPage(), pageBean.getLimit(), keyWord);
        JSONObject jsObj = new JSONObject();
        jsObj.put("msg", "");
        jsObj.put("code", 0);
        jsObj.put("count", pBean.getTotalRecs());
        jsObj.put("data", pBean.getJsArr());
        return jsObj;
    }


    /*查询用户全部信息 分页*/
    @RequestMapping("/getAllstoreList")
    @ResponseBody
    public JSONObject getAllstoreList(int page, int limit, String keyWord) {
        PageBean pageBean = new PageBean();
        pageBean.setPage(page);
        pageBean.setLimit(limit);
        PageBean<StoreUser> pBean = storeService.getPageBean2(pageBean.getPage(), pageBean.getLimit(), keyWord);
        JSONObject jsObj = new JSONObject();
        jsObj.put("msg", "");
        jsObj.put("code", 0);
        jsObj.put("count", pBean.getTotalRecs());
        jsObj.put("data", pBean.getJsArr());
        return jsObj;
    }

    /*查询店铺全部信息 分页*/
    @RequestMapping("/getAllstoreListBySearch")
    @ResponseBody
    public JSONObject getAllstoreListBySearch(int page, int limit, String keyWord,String search) {
        PageBean pageBean = new PageBean();
        pageBean.setPage(page);
        pageBean.setLimit(limit);
        PageBean<StoreUser> pBean = storeService.getPageBean3(pageBean.getPage(), pageBean.getLimit(), keyWord,search);
        JSONObject jsObj = new JSONObject();
        jsObj.put("msg", "");
        jsObj.put("code", 0);
        jsObj.put("count", pBean.getTotalRecs());
        jsObj.put("data", pBean.getJsArr());
        return jsObj;
    }


    @RequestMapping("/deleteStore/{storeId}")
    @ResponseBody
    public String deleteStore(@PathVariable int storeId){

        int i = storeService.deleteByStoreId(storeId);

        if(i == 1){
            return "success";
        }
        return "flag";
    }

    /*审核用户*/
    @RequestMapping("/shenheStore/{storeId}/{shenhe}/{userName}")
    @ResponseBody
    public String shenheStore(@PathVariable int storeId,@PathVariable String shenhe,@PathVariable String userName){

        int shenheCode ; // 店铺状态 1:正常 2:停运 3:申请 4:驳回

        if(shenhe.equals("通过")){
            shenheCode = 1;
            int i=  storeService.shenheStore(storeId,shenheCode);
            int j = userService.updataUserRole(userName);
            if (i>0&&j>0){
                return "success";
            }else {
                return "fail";
            }
        }else{
            int rs = storeService.deleteByStoreId(storeId);
            if (rs>0){
                return "success";
            }else {
                return "fail";
            }
        }

    }
    /*用户申请店家*/
    @RequestMapping("/shenqing/{storeName}/{storeDesc}")
    @ResponseBody
    public String shenqing(@PathVariable String storeName, @PathVariable String storeDesc, HttpSession session){

        int userId = (int) session.getAttribute("userId");
        Store storenum = storeService.selectByUserId(userId);

        if(storenum == null){
            int i = storeService.shenqingStore(storeName,storeDesc,userId);
            if(i == 1){
                return "success";
            }
        }else {
            return "into";
        }

        return "flag";
    }


    @RequestMapping("/getStoreByUser")
    @ResponseBody
    public int getStoreByUser(HttpSession session){
        int userId = (int) session.getAttribute("userId");
        Store store = storeService.selectByUserId(userId);
        return store.getStoreId();
    }

    @RequestMapping("/getStoreInfo")
    @ResponseBody
    public Store getStoreInfo(HttpSession session){
        int userId = (int) session.getAttribute("userId");
        Store store = storeService.selectByUserId(userId);
        return store;
    }

    @RequestMapping("/updateStore/{storeName}/{storeDesc}")
    @ResponseBody
    public String updateStore(@PathVariable String storeName,@PathVariable String storeDesc,HttpSession httpSession){
        int userId = (int) httpSession.getAttribute("userId");
        Store store = storeService.selectByUserId(userId);
        int rs = storeService.updateStoreById(store.getStoreId(),storeName,storeDesc);
        if (rs>0){
            return "success";
        }else {
            return "fail";
        }
    }
    @RequestMapping("/stopOpen/{storeId}")
    @ResponseBody
    public String stopOpen(@PathVariable int storeId){
        List<Goods> goodsList = goodsService.getGoodsStoreId2(storeId);
        if (goodsList.isEmpty()||goodsList.size()==0){
            int rs = storeService.stopOpen(storeId);
            if(rs>0){
                return "success";
            }else {
                return "fail";
            }
        }else{
            int rs = storeService.stopOpen(storeId);
            List<Goods> goodsList1 = goodsService.getGoodsByStoreId(storeId);
            for (Goods goods : goodsList1) {
                List<Cart> cartList = goodsService.getCartByGoods(goods.getGoodsId());
                if (cartList.size()>0||!cartList.isEmpty()){
                    for (Cart cart : cartList) {
                        goodsService.deleteCartByGoods(goods.getGoodsId());
                    }
                }
            }
            for (Goods goods : goodsList1) {
                List<Collect> collectList = collectService.getCollectByGoods(goods.getGoodsId());
                if (collectList.size()>0||!collectList.isEmpty()){
                    for (Collect collect : collectList) {
                        collectService.deleteCollectByGoods(goods.getGoodsId());
                    }
                }
            }
            for (Goods goods : goodsList) {
                List<OrderDetail> orderDetailList = orderDetailService.getByGoodsId(goods.getGoodsId());
                if (orderDetailList.size()>0||!orderDetailList.isEmpty()){
                    for (OrderDetail orderDetail : orderDetailList) {
                        orderDetailService.deleteByGoods(goods.getGoodsId());
                        orderService.deleteByorderId(orderDetail.getDetailOrder());
                    }
                }
            }
            goodsService.deleteGoodsByStoreId(storeId);
            if(rs>0){
                return "success";
            }else {
                return "fail";
            }
        }
    }

    @RequestMapping("/renewOpen/{storeId}")
    @ResponseBody
    public String renewOpen(@PathVariable int storeId){
        List<Goods> goodsList = goodsService.getGoodsStoreId2(storeId);
        if (goodsList.isEmpty()||goodsList.size()==0){
            int rs = storeService.renewOpen(storeId);
            if(rs>0){
                return "success";
            }else {
                return "fail";
            }
        }else{
            int rs = storeService.renewOpen(storeId);
            int rs2 = goodsService.renewGoodsByStoreId(storeId);

            if(rs>0&&rs2>0){
                return "success";
            }else {
                return "fail";
            }
        }
    }
    @RequestMapping("/isStop")
    @ResponseBody
    public String isStop(HttpSession httpSession){
        int userId = (int) httpSession.getAttribute("userId");
        Store store = storeService.selectByUserId(userId);
        String storeState = store.getStoreState();
        if (Integer.parseInt(storeState)==1){
            return "success";
        }else {
            return "fail";
        }
    }
}
