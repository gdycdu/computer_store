package com.gdy.store.controller;

import com.gdy.store.service.BannerService;
import com.gdy.store.pojo.Banner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("CSM")
public class ViewController {
    @Autowired
    private BannerService bannerService;

    //用户登录页面
    @RequestMapping("/toLogin")
    public String tologin(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.removeAttribute("user");
        return "/login";
    }

    //注册页面
    @RequestMapping("/toShowReg")
    public String showAdd() {
        return "/register";
    }

    //管理员登录界面
    @RequestMapping("/toAdminLogin")
    public String toAdminLogin() {
        return "/admin/admin_login";
    }

    //主界面
    @RequestMapping("/toIndex")
    public String toIndex(Model model) {
        List<Banner> bannerList = bannerService.getAllBanner();
        model.addAttribute("bannerList", bannerList);
        return "/index";
    }

    @RequestMapping("/BecomeSeller")
    public String BecomeSeller() {
        return "/BecomeSeller";
    }

    //测试界面
    @RequestMapping("/toTest")
    public String toTest() {
        return "/test";
    }
    //

    @RequestMapping("/toAdminIndex")
    /*管理员主界面*/
    public String toAdminIndex() {
        return "/admin/admin_index";
    }

    @RequestMapping("/toStoreIndex")
    /*管理员主界面*/
    public String toStoreIndex() {
        return "/admin/store_index";
    }

    @RequestMapping("/toNotLogin")
    public String toNotLogin(){
        return "/notLogin";
    }

}
