package com.gdy.store.controller;

import com.gdy.store.pojo.*;
import com.gdy.store.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderDetailService orderDetailService;

    @Autowired
    private GoodsService goodsService;

    @Autowired
    private AddressService addressService;
    @Autowired
    private StoreService storeService;

    /*查询订单信息*/
    @RequestMapping("/toOrderByUserId")
    public String toOrderByUserId(HttpSession session, Model model) throws ParseException {

        int userId = (int) session.getAttribute("userId");

        List<Orderinfo> orderinfoList = orderService.selectOrderByUserId(userId);

        List<Integer> orderDetails2 = new ArrayList<>();

        for (Orderinfo orderinfo1 : orderinfoList ) {
            Long id = orderinfo1.getOrderId();
            OrderDetail orderDetail3 = orderService.selectGoodsIdByOrderId(id);
            orderDetails2.add(orderDetail3.getDetailGoods());
        }


        List<Goods> goods = new ArrayList<>();

        for (Integer integer : orderDetails2) {
            Goods goods1 = goodsService.getGoodsById(integer);
            goods.add(goods1);
        }

        List<orderGoods3> orderGoodsList = new ArrayList<>();

        for (int i = 0;i < orderinfoList.size();i++) {
            Store store = storeService.selectByOrderId(orderinfoList.get(i).getOrderStore());
            orderGoods3 orderGoods = new orderGoods3();
            orderGoods.setOrderId(orderinfoList.get(i).getOrderId());
            orderGoods.setOrderUser(orderinfoList.get(i).getOrderUser());
            orderGoods.setOrderDate(orderinfoList.get(i).getOrderDate());
            orderGoods.setOrderPrice(orderinfoList.get(i).getOrderPrice());
            orderGoods.setOrderState(orderinfoList.get(i).getOrderState());
            orderGoods.setOrderReceiver(orderinfoList.get(i).getOrderReceiver());
            orderGoods.setOrderPhone(orderinfoList.get(i).getOrderPhone());
            orderGoods.setOrderAddress(orderinfoList.get(i).getOrderAddress());
            orderGoods.setCreateTimeOrder(orderinfoList.get(i).getCreateTime());
            orderGoods.setUpdateTimeOrder(orderinfoList.get(i).getUpdateTime());

            orderGoods.setGoodsStore(store.getStoreName());
            orderGoods.setGoodsId(goods.get(i).getGoodsId());
            orderGoods.setGoodsName(goods.get(i).getGoodsName());
            orderGoods.setGoodsNums(goods.get(i).getGoodsNums());
            orderGoods.setGoodsBrand(goods.get(i).getGoodsBrand());
            orderGoods.setGoodsType(goods.get(i).getGoodsType());
            orderGoods.setGoodsPrice(goods.get(i).getGoodsPrice());
            orderGoods.setGoodsDesc(goods.get(i).getGoodsDesc());
            orderGoods.setGoodsSales(goods.get(i).getGoodsSales());
            orderGoods.setGoodsState(goods.get(i).getGoodsState());
            orderGoods.setGoodsImg(goods.get(i).getGoodsImg());
            orderGoods.setCreateTimeGoods(goods.get(i).getCreateTime());

            SimpleDateFormat sf1 = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy", Locale.ENGLISH);
            Date date = sf1.parse(String.valueOf(orderinfoList.get(i).getCreateTime()));
            SimpleDateFormat sf2 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            orderGoods.setUpdateTimeGoods(sf2.format(orderinfoList.get(i).getCreateTime()));

            orderGoodsList.add(orderGoods);
        }
        model.addAttribute("orderinfoList",orderinfoList);
        model.addAttribute("orderGoodsList",orderGoodsList);
        return "/orderUserList";
    }

    /*取消订单*/
    @RequestMapping("/delOrderById/{orderId}")
    @ResponseBody
    public String delCollectById(@PathVariable Long orderId) {
        int b = orderService.deleteDetailByorderId(orderId);
        int i = orderService.deleteByorderId(orderId);
        if (i > 0 && b > 0) {
            return "success";
        } else {
            return "fail";
        }
    }

    @RequestMapping("/getOrderCountByStore")
    @ResponseBody
    public int getOrderCountByStore(HttpSession httpSession){
        int userId = (int) httpSession.getAttribute("userId");
        Store store = storeService.selectByUserId(userId);
        int count = orderService.getOrderCountByStore(store.getStoreId());
        return count;
    }

    @RequestMapping("/getOrderCountWaitDeliver")
    @ResponseBody
    public int getOrderCountWaitDeliver(HttpSession httpSession){
        int userId = (int) httpSession.getAttribute("userId");
        Store store = storeService.selectByUserId(userId);
        int count = orderService.getOrderCountWaitDeliver(store.getStoreId());
        return count;
    }

    @RequestMapping("/getGoodsCountByStore")
    @ResponseBody
    public int getGoodsCountByStore(HttpSession httpSession){
        int userId = (int) httpSession.getAttribute("userId");
        Store store = storeService.selectByUserId(userId);
        int count = goodsService.getGoodsCountByStore(store.getStoreId());
        return count;
    }
    @RequestMapping("/getUserCountByOrder")
    @ResponseBody
    public int getUserCountByOrder(HttpSession httpSession){
        int userId = (int) httpSession.getAttribute("userId");
        Store store = storeService.selectByUserId(userId);
        int count = orderService.getUserCountByOrder(store.getStoreId());
        return count;
    }

    @RequestMapping("/getOrderByStore")
    @ResponseBody
    public String getOrderByStore(HttpSession httpSession){
        int userId = (int) httpSession.getAttribute("userId");
        Store store = storeService.selectByUserId(userId);
        List<Orderinfo> orderinfoList = orderService.getOrderByStoreId(store.getStoreId());
        if (orderinfoList.isEmpty()||orderinfoList.size()==0){
            return "null";
        }else {
            return "have";
        }
    }

}
