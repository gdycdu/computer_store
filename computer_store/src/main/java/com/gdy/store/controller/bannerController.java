package com.gdy.store.controller;

import com.alibaba.fastjson.JSONObject;
import com.gdy.store.service.BannerService;
import com.gdy.store.pojo.Banner;
import com.gdy.store.tools.PageBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

@Controller
@RequestMapping("banner")
public class bannerController {
    @Autowired
    private BannerService bannerService;

    /*管理员获取banner全部信息*/
    @RequestMapping("/getAllBannerByAdmin")
    @ResponseBody
    public JSONObject getAllBannerByAdmin(int page, int limit, String keyWord) {
        PageBean pageBean = new PageBean();
        pageBean.setPage(page);
        pageBean.setLimit(limit);
        PageBean<Banner> pBean = bannerService.getPageBean(pageBean.getPage(), pageBean.getLimit(), keyWord);
        JSONObject jsObj = new JSONObject();
        jsObj.put("msg", "");
        jsObj.put("code", 0);
        jsObj.put("count", pBean.getTotalRecs());
        jsObj.put("data", pBean.getJsArr());
        return jsObj;
    }

    @RequestMapping(value = "/upload")
    @ResponseBody
    public JSONObject upUserImg(@RequestParam(value = "file") MultipartFile file) throws IOException {
        String name = file.getOriginalFilename();
        String path = "D:/IntelliJ IDEA 2022.3.2/workspace/computer_store/src/main/resources/static/img/header/banner/" + name;
        file.transferTo(new File(path));
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("bannerImg", name);
        return jsonObject;
    }

    /*添加轮播信息*/
    @RequestMapping("/addBanner")
    @ResponseBody
    public String addBanner(Banner banner) {
        int rs = bannerService.addBanner(banner);
        if (rs > 0) {
            return "success";
        } else {
            return "fail";
        }
    }

    /*修改轮播信息*/
    @RequestMapping("/updateBanner")
    @ResponseBody
    public String updateBanner(Banner banner) {
        int rs = bannerService.updateBanner(banner);
        if (rs > 0) {
            return "success";
        } else {
            return "fail";
        }
    }

    /*删除轮播信息 通过id*/
    @RequestMapping("/deleteBanner/{bannerId}")
    @ResponseBody
    public String deleteBanner(@PathVariable int bannerId) {
        int rs = bannerService.deleteBanner(bannerId);
        if (rs > 0) {
            return "success";
        } else {
            return "fail";
        }
    }

    /*批量删除轮播信息*/
    @RequestMapping("/batchDeleteBanner")
    @ResponseBody
    public String batchDelete(String batchId) {
        System.out.println(batchId);
        String[] list = batchId.split(",");
        System.out.println(Arrays.toString(list));
        boolean flag = true;
        for (String id : list) {
            int bannerId = Integer.valueOf(id);
            int rs = bannerService.deleteBanner(bannerId);
            if (rs < 0) {
                flag = false;
            }
        }
        if (flag) {
            return "success";
        } else {
            return "fail";
        }
    }

    /*选用轮播图*/
    @RequestMapping("/selectBanner/{bannerSelect}/{bannerId}")
    @ResponseBody
    public String selectBanner(@PathVariable int bannerSelect, @PathVariable int bannerId) {
        int count = bannerService.getSelectedBanner();
        if (bannerSelect == 1 && count < 4) {
            int rs = bannerService.selectBanner(bannerSelect, bannerId);
            if (rs > 0) {
                return "success";
            } else {
                return "fail";
            }
        } else if (bannerSelect == 1 && count >= 4) {
            return "full";
        } else {
            int i = bannerService.selectBanner(bannerSelect, bannerId);
            if (i > 0) {
                return "success";
            } else {
                return "fail";
            }
        }
    }
}
