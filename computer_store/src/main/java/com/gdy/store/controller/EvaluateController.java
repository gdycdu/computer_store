package com.gdy.store.controller;

import com.alibaba.fastjson.JSONObject;
import com.gdy.store.pojo.*;
import com.gdy.store.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.websocket.server.PathParam;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/evaluate")
public class EvaluateController {
    @Autowired
    private EvaImgService evaImgService;
    @Autowired
    private EvaluateService evaluateService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private StoreService storeService;

    @RequestMapping("/uploadEvaImg")
    @ResponseBody
    public JSONObject uploadEvaImg(@PathParam(value = "file") MultipartFile file, HttpServletRequest request) {
        String name = file.getOriginalFilename();
        JSONObject jsonObject = new JSONObject();
        String path = "D:/IntelliJ IDEA 2022.3.2/workspace/computer_store/src/main/resources/static/img/evaImg/" + name;
        try {
            file.transferTo(new File(path));
            jsonObject.put("evaImg", name);
            jsonObject.put("code", 0);
        } catch (IOException e) {
            e.printStackTrace();
            jsonObject.put("code", 1);
        }
        return jsonObject;
    }


    @RequestMapping("/Receiving")
    @ResponseBody
    public String Receiving(@RequestParam(value = "orderId",required = true)Long orderId,
                            @RequestParam(value = "fen",required = true)String fen,
                            @RequestParam(value = "receiving",required = true) String text, HttpSession session){

        int receiving = 0;

        if (fen.equals("1星")){
            receiving = 1;
        }
        if (fen.equals("2星")){
            receiving = 2;
        }
        if (fen.equals("3星")){
            receiving = 3;
        }
        if (fen.equals("4星")){
            receiving = 4;
        }
        if (fen.equals("5星")){
            receiving = 5;
        }

        OrderDetail orderDetail = orderService.selectGoodsIdByOrderId(orderId);
        Orderinfo orderinfo = orderService.selectOrderByOrderId(orderId);
        int userId = (int) session.getAttribute("userId");

        Evaluate evaluate = new Evaluate();
        evaluate.setEvaUser(userId);
        evaluate.setEvaGoods(orderDetail.getDetailGoods());
        evaluate.setEvaContent(text);
        evaluate.setEvaLevel(receiving);
        evaluate.setEvaDate(new Date());
        evaluate.setEvaStore(orderinfo.getOrderStore());
        evaluate.setEvaState(1);

        int i = evaluateService.insertEvaluate(evaluate);
        /*完成订单*/
        int j = goodsService.getGoodsByOK(orderId);

        if(i == 1 && j == 1){
            return "success";
        }else {
            return "flag";
        }
    }
    @RequestMapping("/getAllEva")
    @ResponseBody
    public List<Evaluate2> getAllEva(HttpSession httpSession){
        int userId = (int) httpSession.getAttribute("userId");
        List<Evaluate2> evaluate2List = evaluateService.getAllEva2(userId);
        return evaluate2List;
    }

    @RequestMapping("/getCountWhereO")
    @ResponseBody
    public int getCountWhereO(HttpSession httpSession){
        int userId = (int) httpSession.getAttribute("userId");
        Store store = storeService.selectByUserId(userId);
        int count = evaluateService.getCountWhereO(store.getStoreId());
        return count;
    }

    @RequestMapping("/getCountWhereF")
    @ResponseBody
    public int getCountWhereF(HttpSession httpSession){
        int userId = (int) httpSession.getAttribute("userId");
        Store store = storeService.selectByUserId(userId);
        int count = evaluateService.getCountWhereF(store.getStoreId());
        return count;
    }
}
