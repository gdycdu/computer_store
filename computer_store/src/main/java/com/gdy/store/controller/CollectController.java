package com.gdy.store.controller;

import com.gdy.store.service.CollectService;
import com.gdy.store.service.GoodsService;
import com.gdy.store.pojo.Collect;
import com.gdy.store.pojo.CollectGoods;
import com.gdy.store.pojo.Goods;
import com.gdy.store.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/collect")
public class CollectController {
    @Autowired
    private CollectService collectService;
    @Autowired
    private GoodsService goodsService;

    /*收藏商品*/
    @RequestMapping("/collectGoods/{collectGoods}")
    @ResponseBody
    public String collectGoods(@PathVariable int collectGoods, HttpServletRequest request){
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        /*防止重复*/
        Collect goods = collectService.checkCollect(user.getUserId(), collectGoods);
        if (goods==null){
            int rs = collectService.collectGoods(user.getUserId(), collectGoods);
            if (rs>0){
                return "success";
            }else {
                return "fail";
            }
        }else {
            return "fail_Repeat";
        }
    }
    /*获取用户收藏列表*/
    @RequestMapping("/userCollect")
    public String userCollect(Model model,HttpServletRequest request){
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        List<Collect> allCollectList = collectService.getAllCollect(user.getUserId());
        List<CollectGoods> collectGoodsList = new ArrayList<>();
        for (Collect collectList : allCollectList) {
            CollectGoods collectGoods = new CollectGoods();
            Goods goods = goodsService.getGoodsById(collectList.getCollectGoods());
            collectGoods.setCollectId(collectList.getCollectId());
            collectGoods.setCollectUser(collectList.getCollectUser());
            collectGoods.setCollectState(collectList.getCollectState());
            collectGoods.setGoodsAttr(goods.getGoodsAttr());
            collectGoods.setGoodsId(goods.getGoodsId());
            collectGoods.setGoodsAttr(goods.getGoodsAttr());
            collectGoods.setGoodsName(goods.getGoodsName());
            collectGoods.setGoodsNums(goods.getGoodsNums());
            collectGoods.setGoodsPrice(goods.getGoodsPrice());
            collectGoods.setGoodsImg(goods.getGoodsImg());
            collectGoods.setGoodsState(goods.getGoodsState());
            collectGoodsList.add(collectGoods);
        }
        model.addAttribute("collectGoodsList",collectGoodsList);
        return "collectList";
    }
    /*删除收藏*/
    @RequestMapping("/delCollectById/{collectId}")
    @ResponseBody
    public String delCollectById(@PathVariable int collectId){
        int rs = collectService.delCollectById(collectId);
        if (rs>0){
            return "success";
        }else {
            return "fail";
        }
    }

    /*取消收藏商品*/
    @RequestMapping("/delReCollect/{collectGoods}")
    @ResponseBody
    public String delReCollect(@PathVariable int collectGoods,HttpServletRequest request){
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        Collect goods = collectService.checkCollect(user.getUserId(), collectGoods);
        int rs = collectService.delCollectById(goods.getCollectId());
        if (rs>0){
            return "success";
        }else {
            return "fail";
        }
    }
}
