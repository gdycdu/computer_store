package com.gdy.store.controller;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;

import com.gdy.store.pojo.*;
import com.gdy.store.service.*;
import com.gdy.store.config.AlipayConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("/payment")
public class PayController {

    @Autowired
    private AlipayConfig alipayConfig;

    @Autowired
    private OrderService orderService;

    @Autowired
    private GoodsService goodsService;

    @Autowired
    private EvaluateService evaluateService;

    @Autowired
    private AddressService addressService;

    @Autowired
    private StoreService storeService;

    @Autowired
    private BannerService bannerService;

    /*待付款*/
    @RequestMapping("/getOrderIdByIdBuy/{OrderId}")
    public void  getOrderIdByIdBuy(@PathVariable Long OrderId, Model model, HttpServletResponse response,HttpSession session,HttpServletRequest request)  {

        request.getSession().removeAttribute("orderId3");
        String type = "info"; // 类型
        OrderDetail orderDetail = orderService.selectGoodsIdByOrderId(OrderId);
        Goods goods = goodsService.getGoodsById(orderDetail.getDetailGoods());

        BigDecimal price = goods.getGoodsPrice();
        int price2 = price.intValue() * orderDetail.getDetailNum();

        try {
            AlipayClient alipayClient = new DefaultAlipayClient(
                    alipayConfig.getOpenApiDomain(),
                    alipayConfig.getAppId(),
                    alipayConfig.getMerchantPrivateKey(),
                    alipayConfig.getFormat(),
                    alipayConfig.getCharset(),
                    alipayConfig.getAlipayPublicKey(),
                    alipayConfig.getSignType()); //获得初始化的AlipayClient

            AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();//创建API对应的request
            alipayRequest.setReturnUrl(alipayConfig.getReturnUrl());
            alipayRequest.setNotifyUrl(alipayConfig.getNotifyUrl());//在公共参数中设置回跳和通知地址

            if (type.equals("info")){
                alipayRequest.setBizContent("{" +
                        "    \"out_trade_no\":\""+OrderId+"\"," +
                        "    \"product_code\":\"FAST_INSTANT_TRADE_PAY\"," +
                        "    \"total_amount\":\""+price2+"\"," +
                        "    \"subject\":\""+goods.getGoodsName()+"\"," +
                        "    \"body\":\""+goods.getGoodsName()+"\"," +
                        "    \"passback_params\":\"merchantBizType%3d3C%26merchantBizNo%3d2016010101111\"," +
                        "    \"extend_params\":{" +
                        "    \"sys_service_provider_id\":\"2088511833207846\"" +
                        "    }"+
                        "  }");//填充业务参数
            }else if (type.equals("cart")){
                alipayRequest.setBizContent("{" +
                        "    \"out_trade_no\":\""+OrderId+"\"," +
                        "    \"product_code\":\"FAST_INSTANT_TRADE_PAY\"," +
                        "    \"total_amount\":\""+goods.getGoodsPrice()+"\"," +
                        "    \"subject\":\""+goods.getGoodsName()+"\"," +
                        "    \"body\":\""+goods.getGoodsName()+"\"," +
                        "    \"passback_params\":\"merchantBizType%3d3C%26merchantBizNo%3d2016010101111\"," +
                        "    \"extend_params\":{" +
                        "    \"sys_service_provider_id\":\"2088511833207846\"" +
                        "    }"+
                        "  }");//填充业务参数
            }

            String form="";
            try {
                form = alipayClient.pageExecute(alipayRequest).getBody(); //调用SDK生成表单
            } catch (AlipayApiException e) {
                e.printStackTrace();
            }
            session.setAttribute("type",type);
            response.setContentType("text/html;charset=" + alipayConfig.getCharset());
            response.getWriter().write(form);//直接将完整的表单html输出到页面
            response.getWriter().flush();
            response.getWriter().close();
        } catch (Exception e) {
            e.printStackTrace();
            model.addAttribute("exception", "支付出错了!");
        }
    }

    /*单样商品购买生成订单*/
    @RequestMapping("/payById/{goodsId}/{num}")
    public void  payById(@PathVariable int goodsId, @PathVariable int num, Model model, HttpServletResponse response, HttpServletRequest request)  {
        request.getSession().removeAttribute("orderId3");
        String orderId = "";
        String orderDetailsId = "";
        String type = "info";
        HttpSession session = request.getSession();
        int userId = (int) session.getAttribute("userId");
        Goods goods = goodsService.getGoodsById(goodsId);
        List<Address> address = addressService.getAddrByUserId(userId);
        Address address1 = addressService.getAddrByUserIdByDefault(userId);
        BigDecimal price = goods.getGoodsPrice();
        int price2 = price.intValue() * num;
        BigDecimal price3 = new BigDecimal(price2);

        goods.setGoodsPrice(price3);
        model.addAttribute("address1",address1);
        model.addAttribute("goods",goods);
        model.addAttribute("num",num);

        Long orderId2 = Long.valueOf(getOrderIdByTime());
        Goods goods1 = goodsService.selectStoreByGoodsId(goods.getGoodsId());
        Orderinfo order = new Orderinfo();
        order.setOrderId(orderId2);
        order.setOrderUser(userId);
        order.setOrderDate(new Date());
        order.setOrderPrice(price3);
        /*待付款*/
        order.setOrderState(1);
        order.setOrderReceiver(address1.getAddrReceiver());
        order.setOrderPhone(address1.getAddrPhone());
        order.setOrderAddress(address1.getAddrProvince()+address1.getAddrCity()+address1.getAddrArea()+address1.getAddrStreet()+address1.getAddrDetail());
        order.setCreateTime(new Timestamp(new Date().getTime()));
        order.setUpdateTime(new Timestamp(new Date().getTime()));
        order.setOrderStore(goods1.getGoodsStore());
        orderService.insertOrder(order);

        OrderDetail orderDetail = new OrderDetail();
        orderDetail.setDetailOrder(orderId2);
        orderDetail.setDetailGoods(goods.getGoodsId());
        orderDetail.setDetailPrice(price3.doubleValue());
        orderDetail.setDetailNum(num);
        orderService.insertDetail(orderDetail);

        try {
            AlipayClient alipayClient = new DefaultAlipayClient(
                    alipayConfig.getOpenApiDomain(),
                    alipayConfig.getAppId(),
                    alipayConfig.getMerchantPrivateKey(),
                    alipayConfig.getFormat(),
                    alipayConfig.getCharset(),
                    alipayConfig.getAlipayPublicKey(),
                    alipayConfig.getSignType()); //获得初始化的AlipayClient

            AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();//创建API对应的request
            alipayRequest.setReturnUrl(alipayConfig.getReturnUrl());
            alipayRequest.setNotifyUrl(alipayConfig.getNotifyUrl());//在公共参数中设置回跳和通知地址

            if (type.equals("info")){
               /* Order order = orderService.selectByOrderIdAndOrderDetailsId(orderId,orderDetailsId);*/
                alipayRequest.setBizContent("{" +
                        "    \"out_trade_no\":\""+orderId2+"\"," +
                        "    \"product_code\":\"FAST_INSTANT_TRADE_PAY\"," +
                        "    \"total_amount\":\""+price2+"\"," +
                        "    \"subject\":\""+goods.getGoodsName()+"\"," +
                        "    \"body\":\""+goods.getGoodsName()+"\"," +
                        "    \"passback_params\":\"merchantBizType%3d3C%26merchantBizNo%3d2016010101111\"," +
                        "    \"extend_params\":{" +
                        "    \"sys_service_provider_id\":\"2088511833207846\"" +
                        "    }"+
                        "  }");//填充业务参数
            }else if (type.equals("cart")){
              /*  OrderDetails orderDetails = detailsService.selectByPrimaryKey(orderId);*/
                alipayRequest.setBizContent("{" +
                        "    \"out_trade_no\":\""+orderId2+"\"," +
                        "    \"product_code\":\"FAST_INSTANT_TRADE_PAY\"," +
                        "    \"total_amount\":\""+price2+"\"," +
                        "    \"subject\":\""+goods.getGoodsName()+"\"," +
                        "    \"body\":\""+goods.getGoodsName()+"\"," +
                        "    \"passback_params\":\"merchantBizType%3d3C%26merchantBizNo%3d2016010101111\"," +
                        "    \"extend_params\":{" +
                        "    \"sys_service_provider_id\":\"2088511833207846\"" +
                        "    }"+
                        "  }");//填充业务参数
            }
            String form="";
            try {
                form = alipayClient.pageExecute(alipayRequest).getBody(); //调用SDK生成表单
            } catch (AlipayApiException e) {
                e.printStackTrace();
            }
            session.setAttribute("type",type);
            response.setContentType("text/html;charset=" + alipayConfig.getCharset());
            response.getWriter().write(form);//直接将完整的表单html输出到页面
            response.getWriter().flush();
            response.getWriter().close();
        } catch (Exception e) {
            e.printStackTrace();
            model.addAttribute("exception", "支付出错了!");
        }
    }

    /*结算*/
    @RequestMapping("/pay")
    public void  pay( Model model, HttpServletResponse response, HttpServletRequest request)  {
        String orderId = "";
        String orderDetailsId = "";
        String type = "info";
        HttpSession session = request.getSession();
        // CartId集合
        List Classifys = (List) session.getAttribute("Classifys");
        // 商品数量集合
        List num = (List) session.getAttribute("num");
        int userId = (int) session.getAttribute("userId");
        List<Integer> goodsId = new ArrayList();
        BigDecimal Allprice = new BigDecimal("0");
        /*通过cartId拿到goodsId*/
        for(int i =0;i<Classifys.size();i++){
            String  a = (String) Classifys.get(i);
            Integer b = Integer.parseInt(a);
            Cart cart = goodsService.getGoodsIdByCartId(b);
            goodsId.add(cart.getCartGoods());
        }
        List<Goods> goodsList = new ArrayList<>();
        /*通过goodsId拿到goods*/
        for(int i =0;i<goodsId.size();i++) {
            Integer b = goodsId.get(i);
            Goods goods = goodsService.getGoodsById(b);
            goodsList.add(goods);
        }
        // 根据商品集合,获取总价
        for(int j=0;j<goodsList.size();j++) {
            BigDecimal price = goodsList.get(j).getGoodsPrice();
            String num2 = (String) num.get(j);
            Integer num3 = Integer.parseInt(num2);
            int price2 = price.intValue() * num3;
            BigDecimal price3 = new BigDecimal(price2);
            goodsList.get(j).setGoodsPrice(price3);
            Allprice = Allprice.add(price3);
        }
        // 数量
        for (int c =0;c<num.size();c++){
            String  a = (String) num.get(c);
            Integer b = Integer.parseInt(a);
            goodsList.get(c).setGoodsNums(b);
        }
        // 定义订单id
        Long orderId2 = null;
        String goodsName = "";
        List<Long> orderId3 = new ArrayList<>();
        //循环遍历生成订单
        for(int c=0;c < goodsList.size();c++){
            Goods goods = goodsService.getGoodsById(goodsList.get(c).getGoodsId());
            goodsName = goodsName + goods.getGoodsName() + " ";
            List<Address> address = addressService.getAddrByUserId(userId);
            Address address1 = addressService.getAddrByUserIdByDefault(userId);
            goods.setGoodsPrice(goodsList.get(c).getGoodsPrice());
            model.addAttribute("address1",address1);
            model.addAttribute("goods",goods);
            model.addAttribute("num",num);
            orderId2 = Long.valueOf(getOrderIdByTime());
            Goods goods1 = goodsService.selectStoreByGoodsId(goods.getGoodsId());
            Orderinfo order = new Orderinfo();
            order.setOrderId(orderId2);
            order.setOrderUser(userId);
            order.setOrderDate(new Date());
            order.setOrderPrice(goodsList.get(c).getGoodsPrice());
            order.setOrderState(1);
            order.setOrderReceiver(address1.getAddrReceiver());
            order.setOrderPhone(address1.getAddrPhone());
            order.setOrderAddress(address1.getAddrProvince()+address1.getAddrCity()+address1.getAddrArea()+address1.getAddrStreet()+address1.getAddrDetail());
            order.setCreateTime(new Timestamp(new Date().getTime()));
            order.setUpdateTime(new Timestamp(new Date().getTime()));
            order.setOrderStore(goods1.getGoodsStore());
            orderService.insertOrder(order);
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.setDetailOrder(orderId2);
            orderDetail.setDetailGoods(goods.getGoodsId());
            BigDecimal bigDecimal = goodsList.get(c).getGoodsPrice();
            orderDetail.setDetailPrice(bigDecimal.doubleValue());
            orderDetail.setDetailNum(goodsList.get(c).getGoodsNums());
            orderService.insertDetail(orderDetail);

            orderId3.add(orderId2);
        }
        session.setAttribute("orderId3",orderId3);
        try {
            AlipayClient alipayClient = new DefaultAlipayClient(
                    alipayConfig.getOpenApiDomain(),
                    alipayConfig.getAppId(),
                    alipayConfig.getMerchantPrivateKey(),
                    alipayConfig.getFormat(),
                    alipayConfig.getCharset(),
                    alipayConfig.getAlipayPublicKey(),
                    alipayConfig.getSignType()); //获得初始化的AlipayClient

            AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();//创建API对应的request
            alipayRequest.setReturnUrl(alipayConfig.getReturnUrl());
            alipayRequest.setNotifyUrl(alipayConfig.getNotifyUrl());//在公共参数中设置回跳和通知地址

            if (type.equals("info")){
                /* Order order = orderService.selectByOrderIdAndOrderDetailsId(orderId,orderDetailsId);*/
                alipayRequest.setBizContent("{" +
                        "    \"out_trade_no\":\""+orderId2+"\"," +
                        "    \"product_code\":\"FAST_INSTANT_TRADE_PAY\"," +
                        "    \"total_amount\":\""+Allprice+"\"," +
                        "    \"subject\":\""+goodsName+"\"," +
                        "    \"body\":\""+goodsName+"\"," +
                        "    \"passback_params\":\"merchantBizType%3d3C%26merchantBizNo%3d2016010101111\"," +
                        "    \"extend_params\":{" +
                        "    \"sys_service_provider_id\":\"2088511833207846\"" +
                        "    }"+
                        "  }");//填充业务参数
            }else if (type.equals("cart")){
                /*  OrderDetails orderDetails = detailsService.selectByPrimaryKey(orderId);*/
                alipayRequest.setBizContent("{" +
                        "    \"out_trade_no\":\""+orderId2+"\"," +
                        "    \"product_code\":\"FAST_INSTANT_TRADE_PAY\"," +
                        "    \"total_amount\":\""+Allprice+"\"," +
                        "    \"subject\":\""+goodsName+"\"," +
                        "    \"body\":\""+goodsName+"\"," +
                        "    \"passback_params\":\"merchantBizType%3d3C%26merchantBizNo%3d2016010101111\"," +
                        "    \"extend_params\":{" +
                        "    \"sys_service_provider_id\":\"2088511833207846\"" +
                        "    }"+
                        "  }");//填充业务参数
            }

            String form="";
            try {
                form = alipayClient.pageExecute(alipayRequest).getBody(); //调用SDK生成表单
            } catch (AlipayApiException e) {
                e.printStackTrace();
            }
            session.setAttribute("type",type);
            response.setContentType("text/html;charset=" + alipayConfig.getCharset());
            response.getWriter().write(form);//直接将完整的表单html输出到页面
            response.getWriter().flush();
            response.getWriter().close();
        } catch (Exception e) {
            e.printStackTrace();
            model.addAttribute("exception", "支付出错了!");
        }
    }
    /**/
    @RequestMapping("/return")
    public String returnUrl(Long out_trade_no,String total_amount,String body,
                            Model model,HttpSession session) throws ParseException {

        List<Long> orderId3 = (List<Long>) session.getAttribute("orderId3");
        if(orderId3 != null){
            for (Long orderId : orderId3){
                Orderinfo orderinfo = new Orderinfo();
                orderinfo.setOrderId(orderId);
                orderinfo.setOrderState(2);/*已支付*/
                orderService.updataByno(orderinfo);
                OrderDetail orderDetail = goodsService.selectGoodIdByOrderId(orderId);
                goodsService.updataGoodsNumsAndSales(orderDetail.getDetailGoods(),orderDetail.getDetailNum());
            }
        }else{
            Orderinfo orderinfo = new Orderinfo();
            orderinfo.setOrderId(out_trade_no);
            orderinfo.setOrderState(2);
            orderService.updataByno(orderinfo);
            OrderDetail orderDetail = goodsService.selectGoodIdByOrderId(out_trade_no);
            goodsService.updataGoodsNumsAndSales(orderDetail.getDetailGoods(),orderDetail.getDetailNum());
        }
        /*返回到用户订单*/
        List<Banner> bannerList = bannerService.getAllBanner();
        model.addAttribute("bannerList", bannerList);
        return "/index";
    }

    @RequestMapping("/notify")
    public void notifyUrl(){
        System.out.println("notify--------------------------");
    }

    public static String getOrderIdByTime() {
        SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
        String newDate=sdf.format(new Date());
        String result="";
        Random random=new Random();
        for(int i=0;i<3;i++){
            result+=random.nextInt(10);
        }
         return newDate+result;
    }

}
