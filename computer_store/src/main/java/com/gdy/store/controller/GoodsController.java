package com.gdy.store.controller;


import com.alibaba.fastjson.JSONObject;
import com.gdy.store.pojo.*;
import com.gdy.store.service.*;
import com.gdy.store.mapper.StoreMapper;

import com.gdy.store.tools.PageBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("/goods")
public class GoodsController {
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private EvaluateService evaluateService;
    @Autowired
    private AddressService addressService;
    @Autowired
    private GoodsTypeService goodsTypeService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private UserService userService;
    @Autowired
    private BrandService brandService;
    @Autowired
    private StoreMapper storeMapper;
    @Autowired
    private StoreService storeService;

    /*获得全部商品*/
    @RequestMapping("/getAllGoods")
    public String getAllGoods(Model model) {
        List<Goods> allGoodsList = goodsService.getAllGoods();
        model.addAttribute("allGoodsList", allGoodsList);
        return "/goodsList";
    }

    /*模糊查询*/
    @RequestMapping("/getGoodsByLikeName/{goodsName}")
    public String getGoodsByLikeName(@PathVariable String goodsName,Model model){
        List<Goods> allGoodsList = goodsService.getGoodsByLikeName(goodsName);
        model.addAttribute("allGoodsList",allGoodsList);
        return "/goodsList";
    }

    /*根据goods_Id获得商品*/
    @RequestMapping("/getGoodsById/{goodsId}")
    public String getGoodsById(@PathVariable int goodsId, Model model)  throws ParseException {
        Goods goods = goodsService.getGoodsById(goodsId);
        if (goods==null){
            return "/error";
        }
        Store store = storeService.getByStoreId(goods.getGoodsStore());
        GoodsStore goodsStore = new GoodsStore();
        goodsStore.setGoodsId(goods.getGoodsId());
        goodsStore.setGoodsName(goods.getGoodsName());
        goodsStore.setGoodsAttr(goods.getGoodsAttr());
        goodsStore.setGoodsBrand(goods.getGoodsBrand());
        goodsStore.setGoodsDesc(goods.getGoodsDesc());
        goodsStore.setGoodsNums(goods.getGoodsNums());
        goodsStore.setGoodsStore(goods.getGoodsStore());
        goodsStore.setGoodsType(goods.getGoodsType());
        goodsStore.setGoodsImg(goods.getGoodsImg());
        goodsStore.setGoodsSales(goods.getGoodsSales());
        goodsStore.setGoodsPrice(goods.getGoodsPrice());
        goodsStore.setStoreName(store.getStoreName());
        List<Evaluate> evaluateList = evaluateService.getEvaByGoodsId(goodsId);

        List<Evaluate3> evaluate3List = new ArrayList<>();
        for (Evaluate evaluate : evaluateList) {
            User user = userService.selectUserByUserId(evaluate.getEvaUser());
            Evaluate3 evaluate3 = new Evaluate3();
            evaluate3.setEvaId(evaluate.getEvaId());
            evaluate3.setEvaUser(evaluate.getEvaUser());
            evaluate3.setEvaGoods(evaluate.getEvaGoods());
            evaluate3.setEvaContent(evaluate.getEvaContent());
            evaluate3.setEvaLevel(evaluate.getEvaLevel());

            SimpleDateFormat sf1 = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy", Locale.ENGLISH);
            Date date = sf1.parse(String.valueOf(evaluate.getEvaDate()));
            SimpleDateFormat sf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            evaluate3.setEvaDate(sf2.format(date));
            evaluate3.setEvaStore(evaluate.getEvaStore());
            evaluate3.setEvaState(evaluate.getEvaState());
            evaluate3.setUser_Img(user.getUserImg());
            evaluate3List.add(evaluate3);
        }

        int typeId = goods.getGoodsType();
        model.addAttribute("goods", goodsStore);
        model.addAttribute("typeId", typeId);
        model.addAttribute("evaluateList", evaluate3List);
        return "/goodsBuy";
    }

    /*订单详情页*/
    @RequestMapping("/getGoodsByIdBuy/{goodsId}/{num}")
    public String getGoodsByIdBuy(@PathVariable int goodsId,@PathVariable int num, Model model, HttpServletRequest request) {
        HttpSession session = request.getSession();
        int userId = (int) session.getAttribute("userId");
        Goods goods = goodsService.getGoodsById(goodsId);
        List<Address> address = addressService.getAddrByUserId(userId);
        Address address1 = addressService.getAddrByUserIdByDefault(userId);
        BigDecimal price = goods.getGoodsPrice();
        int price2 = price.intValue() * num;
        BigDecimal price3 = new BigDecimal(price2);
        goods.setGoodsPrice(price3);
        model.addAttribute("addressList",address);
        model.addAttribute("address1",address1);
        model.addAttribute("goods",goods);
        model.addAttribute("goodsId",goodsId);
        model.addAttribute("num",num);
        return "/order";
    }

    /*购物车页面*/
    @RequestMapping("/cart")
    public String cart(Model model,HttpSession session){

        int userId = (int) session.getAttribute("userId");
        List<Cart> cartList = goodsService.getCartByUserId(userId);

        List<Goods> goodsList = new ArrayList<>();
        for (int i=0;i<cartList.size();i++){
            Goods goods1 = goodsService.getGoodsById(cartList.get(i).getCartGoods());
            goodsList.add(goods1);
        }
        List<GoodsType> goodsTypes = new ArrayList<>();
        for (int i=0;i<goodsList.size();i++){
            GoodsType goodsType = goodsTypeService.getTypeByTypeId(goodsList.get(i).getGoodsType());
            goodsTypes.add(goodsType);
        }
        List<CartGoods2> cartGoodsList = new ArrayList<>();
        for (int i=0;i<cartList.size();i++){
            CartGoods2 cartGoods = new CartGoods2();
            cartGoods.setCartId(cartList.get(i).getCartId());
            cartGoods.setCartGoods(cartList.get(i).getCartGoods());
            cartGoods.setCartNum(cartList.get(i).getCartNum());
            cartGoods.setCartPrice(cartList.get(i).getCartPrice());
            cartGoods.setCartUser(cartList.get(i).getCartUser());
            cartGoods.setGoodsId(goodsList.get(i).getGoodsId());
            cartGoods.setGoodsName(goodsList.get(i).getGoodsName());
            cartGoods.setGoodsNums(goodsList.get(i).getGoodsNums());
            cartGoods.setGoodsAttr(goodsList.get(i).getGoodsAttr());
            cartGoods.setGoodsImg(goodsList.get(i).getGoodsImg());
            cartGoods.setTypeId(goodsTypes.get(i).getTypeId());
            cartGoods.setTypeName(goodsTypes.get(i).getTypeName());
            BigDecimal num = new BigDecimal( cartList.get(i).getCartNum());
            BigDecimal price = cartList.get(i).getCartPrice();
            BigDecimal allPrice = price.multiply(num);
            cartGoods.setAllcartNum(allPrice);
            cartGoodsList.add(cartGoods);
        }

        model.addAttribute("cartGoodsList",cartGoodsList);
        return "/cart";
    }


    /*获取全部商品信息 分页*/
    @RequestMapping("/getAllGoodsByStoreId")
    @ResponseBody
    public JSONObject getAllGoodsByStoreId(int page, int limit, String keyWord, HttpSession session) {
        int userId = (int) session.getAttribute("userId");
        PageBean pageBean = new PageBean();
        pageBean.setPage(page);
        pageBean.setLimit(limit);
        PageBean<GoodsIntact> pBean = goodsService.getPageBean(pageBean.getPage(), pageBean.getLimit(), keyWord,userId);
        JSONObject jsObj = new JSONObject();
        jsObj.put("msg", "");
        jsObj.put("code", 0);
        jsObj.put("count", pBean.getTotalRecs());
        jsObj.put("data", pBean.getJsArr());
        return jsObj;
    }


    /*获取全部商品信息 分页*/
    @RequestMapping("/getAllGoodsByStoreIdAndGoodId")
    @ResponseBody
    public JSONObject getAllGoodsByStoreIdAndGoodId(int page, int limit, String keyWord,String search, HttpSession session) {
        int userId = (int) session.getAttribute("userId");
        PageBean pageBean = new PageBean();
        pageBean.setPage(page);
        pageBean.setLimit(limit);
        PageBean<GoodsIntact> pBean = goodsService.getPageBean2(pageBean.getPage(), pageBean.getLimit(), keyWord,search,userId);
        JSONObject jsObj = new JSONObject();
        jsObj.put("msg", "");
        jsObj.put("code", 0);
        jsObj.put("count", pBean.getTotalRecs());
        jsObj.put("data", pBean.getJsArr());
        return jsObj;
    }


    /*获取全部订单信息分页*/
    @RequestMapping("/getAllOrdersByStoreId")
    @ResponseBody
    public JSONObject getAllGoodsByStoreId(Integer page, Integer limit, String keyWord, HttpSession session) {
        int userId = (int) session.getAttribute("userId");

        PageBean pageBean = new PageBean();
        pageBean.setPage(page);
        pageBean.setLimit(limit);
        PageBean<orderGoods2> pBean = orderService.getPageBean(pageBean.getPage(), pageBean.getLimit(), keyWord,userId);
        JSONObject jsObj = new JSONObject();
        jsObj.put("msg", "");
        jsObj.put("code", 0);
        jsObj.put("count", pBean.getTotalRecs());
        jsObj.put("data", pBean.getJsArr());
        return jsObj;
    }


    /*获取订单信息 搜索 分页*/
    @RequestMapping("/getAllOrdersByStoreId2")
    @ResponseBody
    public JSONObject getAllGoodsByStoreId2(Integer page, Integer limit, String keyWord,String search, HttpSession session) {
        int userId = (int) session.getAttribute("userId");
        PageBean pageBean = new PageBean();
        pageBean.setPage(page);
        pageBean.setLimit(limit);
        PageBean<orderGoods2> pBean = orderService.getPageBean2(pageBean.getPage(), pageBean.getLimit(), keyWord,search,userId);
        JSONObject jsObj = new JSONObject();
        jsObj.put("msg", "");
        jsObj.put("code", 0);
        jsObj.put("count", pBean.getTotalRecs());
        jsObj.put("data", pBean.getJsArr());
        return jsObj;
    }


    /*获取全部评论信息 分页*/
    @RequestMapping("/getAllEvaluateByStoreId")
    @ResponseBody
    public JSONObject getAllEvaluateByStoreId(Integer page, Integer limit, String keyWord, HttpSession session) {

        int userId = (int) session.getAttribute("userId");

        PageBean pageBean = new PageBean();
        pageBean.setPage(page);
        pageBean.setLimit(limit);
        PageBean<Evaluate2> pBean = evaluateService.getPageBean(pageBean.getPage(), pageBean.getLimit(), keyWord,userId);
        JSONObject jsObj = new JSONObject();
        jsObj.put("msg", "");
        jsObj.put("code", 0);
        jsObj.put("count", pBean.getTotalRecs());
        jsObj.put("data", pBean.getJsArr());
        return jsObj;
    }

    /*通过商品获取全部评论信*/
    @RequestMapping("/getAllCommByGoodsId")
    @ResponseBody
    public JSONObject getAllByGoodsId(Integer page, Integer limit, String keyWord, String search,HttpSession session) {
        System.out.println(search);
        int userId = (int) session.getAttribute("userId");
        PageBean pageBean = new PageBean();
        pageBean.setPage(page);
        pageBean.setLimit(limit);
        PageBean<Evaluate2> pBean = evaluateService.getPageBean2(pageBean.getPage(), pageBean.getLimit(), Integer.parseInt(search),userId);
        JSONObject jsObj = new JSONObject();
        jsObj.put("msg", "");
        jsObj.put("code", 0);
        jsObj.put("count", pBean.getTotalRecs());
        jsObj.put("data", pBean.getJsArr());
        return jsObj;
    }

    /*下架商品*/
    @RequestMapping("/deleteGood")
    @ResponseBody
    public String deleteGood(@RequestParam(value = "goodsId",required = true)int goodsId){

        int i =goodsService.deleteGoodsByGoodsId(goodsId);

        if(i == 1){
            return "success";
        }else {
            return "flag";
        }
    }
    /*上架商品*/
    @RequestMapping("/renewGoods/{goodsId}")
    @ResponseBody
    public String renewGoods(@PathVariable int goodsId){
        int rs = goodsService.renewGoods(goodsId);
        if (rs>0){
            return "success";
        }else {
            return "fail";
        }
    }

    /*发货*/
    @RequestMapping("/updateGoodsByDelivery")
    @ResponseBody
    public  String updateGoodsByDelivery(@RequestParam(value = "id",required = true)Long id){

        int i = goodsService.updateGoodsByDelivery(id);
        if(i == 1){
            return "success";
        }else {
            return "flag";
        }
    }
    /*确认收货*/
    @RequestMapping("/getGoodsByIdReceiving")
    @ResponseBody
    public String getGoodsByIdReceiving(@RequestParam(value = "orderId",required = true)Long id){

        int i = goodsService.getGoodsByIdReceiving(id);

        if(i == 1){
            return "success";
        }else {
            return "flag";
        }
    }

    @RequestMapping("/getAllBrand")
    @ResponseBody
    public List<Brand> getAllBrand(){
        List<Brand> brandList = brandService.getAllBrand();
        return brandList;
    }
    @RequestMapping("/getAllGoodsType")
    @ResponseBody
    public List<GoodsType> getAllGoodsType(){
        List<GoodsType> goodsTypeList = goodsTypeService.getAllGoodsType();
        return goodsTypeList;
    }
    @RequestMapping("/getGoodsByBrand/{brandId}")
    public String getGoodsByBrand(@PathVariable int brandId,Model model){
        List<Goods> allGoodsList = goodsService.getGoodsByBrand(brandId);
        model.addAttribute("allGoodsList", allGoodsList);
        return "/goodsList";
    }


    @RequestMapping("/getGoodsByType/{typeId}")
    public String getGoodsByType(@PathVariable int typeId,Model model){
        List<Goods> allGoodsList = goodsService.getGoodsByType(typeId);
        model.addAttribute("allGoodsList", allGoodsList);
        return "/goodsList";
    }

    @RequestMapping("/toAddGoods")
    public String toAddGoods(){
        return "/admin/addGoods";
    }

    @RequestMapping(value = "/upload")
    @ResponseBody
    public JSONObject upUserImg(@RequestParam(value = "file") MultipartFile file) throws IOException {
        String name = file.getOriginalFilename();
        System.out.println(name);

        String path = "D:/IntelliJ IDEA 2022.3.2/workspace/computer_store/src/main/resources/static/img/header/goods/" + name;
        file.transferTo(new File(path));
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("goodsImg", name);
        return jsonObject;
    }
    /*添加商品*/
    @RequestMapping("/addGoods")
    @ResponseBody
    public String addGoods(Goods goods){
        int rs = goodsService.addGoods(goods);
        if (rs>0){
            return "success";
        }else {
            return "fail";
        }
    }
    /*修改商品信息*/
    @RequestMapping("/updateGoods")
    @ResponseBody
    public String updateGoods(Goods goods){
        int rs = goodsService.updateGoods(goods);
        if (rs>0){
            return "success";
        }else {
           return "fail";
        }
    }
    /*通过店家获取商品*/
    @RequestMapping("/getAllGoodsByStore")
    @ResponseBody
    public List<Goods> getByStore(HttpSession httpSession){
        int userId = (int) httpSession.getAttribute("userId");
        Store store = storeService.selectByUserId(userId);
        List<Goods> goodsList = goodsService.getGoodsByStoreId(store.getStoreId());
        return goodsList;
    }
}
