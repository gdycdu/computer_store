package com.gdy.store.controller;

import com.alibaba.fastjson.JSONObject;
import com.gdy.store.service.StoreService;
import com.gdy.store.service.UserService;
import com.gdy.store.pojo.User;
import com.gdy.store.tools.MD5Util;
import com.gdy.store.tools.PageBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;

@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private StoreService storeService;

    //用户登录
    @RequestMapping("/user_login")
    @ResponseBody
    public String login(User user, HttpServletRequest request, HttpServletResponse response, Model model) {
        String userName = user.getUserName();
        if (userName == null) {
            userName = user.getUserEmail();
            if (userName == null) {
                userName = user.getUserPhone();
            }
        }
        String em = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
        String ph = "^((13[0-9])|(15[^4,\\D])|(17[0-9])|(18[0,5-9]))\\d{8}$";
        if (userName.matches(ph)) {
            user.setUserName(null);
            user.setUserPhone(userName);
        } else if (userName.matches(em)) {
            user.setUserName(null);
            user.setUserEmail(userName);
        }
        System.out.println(user);
        user.setUserPwd(MD5Util.PwdMD5(user.getUserPwd()));
        User user_login = userService.userLogin(user);
        if (user_login != null) {
            if (user_login.getUserState() == 1) {
                HttpSession session = request.getSession();
                session.setAttribute("user", user_login);
                session.setAttribute("userId", user_login.getUserId());
                return "ok";
            }
            if (user_login.getUserState() == 2) {
                return "not";
            } else {
                return "delete";
            }
        } else {
            return "fail";
        }
    }

    //注册
    @RequestMapping("/registerUser")
    @ResponseBody
    public String register(User user) {
        user.setUserPwd(MD5Util.PwdMD5(user.getUserPwd()));
        int rs = userService.addUser(user);
        if (rs > 0) {
            return "success";
        } else {
            return "fail";
        }
    }

    //修该个人信息
    @RequestMapping("/updateUser")
    @ResponseBody
    public String updateUser(User user, HttpServletRequest request) {
        int rs = userService.updateUser(user);
        user = userService.getById(user.getUserId());
        HttpSession session = request.getSession();
        session.setAttribute("user", user);
        if (rs > 0) {
            return "success";
        } else {
            return "fail";
        }
    }

    //管理员修该个人信息
    @RequestMapping("/adminUpdateUser")
    @ResponseBody
    public String adminUpdateUser(User user, HttpServletRequest request) {
        int rs = userService.updateUser(user);
        user = userService.getById(user.getUserId());
        if (rs > 0) {
            return "success";
        } else {
            return "fail";
        }
    }

    //按用户名查询
    @RequestMapping("/checkByName/{userName}")
    @ResponseBody
    public String checkByName(@PathVariable String userName) {
        String msg = "";
        User user = userService.getByName(userName);
        if (user == null) {
            msg = "ok";
        } else {
            msg = "fail";
        }
        return msg;
    }

    @RequestMapping("/checkByName2/{userName}/{userId}")
    @ResponseBody
    public String checkByName2(@PathVariable String userName, @PathVariable int userId) {
        String msg = "";
        User user = userService.getByName2(userName, userId);
        if (user == null) {
            msg = "ok";
        } else {
            msg = "fail";
        }
        return msg;
    }

    //按用户电话号码查询
    @RequestMapping("/checkByPhone/{userPhone}")
    @ResponseBody
    public String checkByPhone(@PathVariable String userPhone) {
        String msg = "";
        User user = userService.getByPhone(userPhone);
        if (user == null) {
            msg = "ok";
        } else {
            msg = "fail";
        }
        return msg;
    }

    @RequestMapping("/checkByPhone2/{userPhone}/{userId}")
    @ResponseBody
    public String checkByPhone2(@PathVariable String userPhone, @PathVariable int userId) {
        String msg = "";
        User user = userService.getByPhone2(userPhone, userId);
        if (user == null) {
            msg = "ok";
        } else {
            msg = "fail";
        }
        return msg;
    }

    //按用户邮箱查询
    @RequestMapping("/checkByEmail/{userEmail}")
    @ResponseBody
    public String checkByEmail(@PathVariable String userEmail) {
        String msg = "";
        User user = userService.getByEmail(userEmail);
        if (user == null) {
            msg = "ok";
        } else {
            msg = "fail";
        }
        return msg;
    }

    @RequestMapping("/checkByEmail2/{userEmail}/{userId}")
    @ResponseBody
    public String checkByEmail2(@PathVariable String userEmail, @PathVariable int userId) {
        String msg = "";
        User user = userService.getByEmail2(userEmail, userId);
        if (user == null) {
            msg = "ok";
        } else {
            msg = "fail";
        }
        return msg;
    }

    //按id查询用户，去修改页面
    @RequestMapping("/getById/{userId}")
    public String getById(@PathVariable int userId, Model model) {
        User user = userService.getById(userId);
        model.addAttribute("user", user);
        return "/user/user_update";
    }

    //验证密码
    @RequestMapping("/verifyPwd/{oldPwd}")
    @ResponseBody
    public String verifyPwd(@PathVariable String oldPwd, HttpServletRequest request) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        System.out.println(MD5Util.PwdMD5(user.getUserPwd()));
        if (MD5Util.PwdMD5(oldPwd).equals(user.getUserPwd())) {
            return "success";
        } else {
            return "fail";
        }
    }

    /*去用户修改密码页面*/
    @RequestMapping("/toUpPwd")
    public String toUpUserPwd() {
        return "/user/user_upPwd";
    }

    //修改密码
    @RequestMapping("/updatePwd/{userPwd}")
    @ResponseBody
    public String upUserPwd(@PathVariable String userPwd, HttpServletRequest request) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        System.out.println(userPwd);
        user.setUserPwd(MD5Util.PwdMD5(userPwd));
        int rs = userService.updatePwd(user);
        if (rs > 0) {
            return "success";
        } else {
            return "fail";
        }
    }

    //上传头像
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject upUserImg(@RequestParam(value = "file") MultipartFile file) throws IOException {
        /*List<String> list = Arrays.asList("jpg","jpeg", "png", "bmp", "gif");
        ArrayList<String> imglist = new ArrayList<>();
        imglist.addAll(list);*/
        String name = file.getOriginalFilename();
      /*  String fileSuffix = name.substring(name.lastIndexOf(".") + 1).toLowerCase();
        System.out.println(fileSuffix);*/

       /* String path = ResourceUtils.getURL("classpath:").getPath()+"static/img/header/"+name;
        String path2 = ClassUtils.getDefaultClassLoader().getResource("static").getPath()+"/img/header/"+name;*/
        String path = "D:/IntelliJ IDEA 2022.3.2/workspace/computer_store/src/main/resources/static/img/header/" + name;
        file.transferTo(new File(path));
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("userImg", name);
        return jsonObject;
    }

    //添加收货地址
    @RequestMapping("/toAddAddress")
    public String toUpAddress() {
        return "/user/user_address";
    }

    //注销 退出登录
    @RequestMapping("/user_logout")
    @ResponseBody
    public String logout(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.removeAttribute("user");
        return "success";
    }

    /*查询用户全部信息 分页*/
    @RequestMapping("/getAllUser")
    @ResponseBody
    public JSONObject getAllUser(int page, int limit, String keyWord) {
        PageBean pageBean = new PageBean();
        pageBean.setPage(page);
        pageBean.setLimit(limit);
        PageBean<User> pBean = userService.getPageBean(pageBean.getPage(), pageBean.getLimit(), keyWord);
        JSONObject jsObj = new JSONObject();
        jsObj.put("msg", "");
        jsObj.put("code", 0);
        jsObj.put("count", pBean.getTotalRecs());
        jsObj.put("data", pBean.getJsArr());
        return jsObj;
    }

    /*删除用户*/
    @RequestMapping("/deleteUser/{userId}")
    @ResponseBody
    public String deleUser(@PathVariable int userId) {
        int rs = userService.deleteUser(userId);
        if (rs > 0) {
            return "success";
        } else {
            return "fail";
        }
    }

    /*批量删除用户*/
    @RequestMapping("/batchDeleteUser")
    @ResponseBody
    public String batchDelete(String batchId) {
        String[] list = batchId.split(",");
        boolean flag = true;
        for (String id : list) {
            int userId = Integer.valueOf(id);
            int rs = userService.deleteUser(userId);
            if (rs < 0) {
                flag = false;
            }
        }
        if (flag) {
            return "success";
        } else {
            return "fail";
        }
    }

    /*改变用户状态*/
    @RequestMapping("/changeUserState/{userState}/{userId}")
    @ResponseBody
    public String changeUserState(@PathVariable int userState, @PathVariable int userId) {
        int rs = userService.changeUserState(userState, userId);
        if (rs > 0) {
            return "success";
        } else {
            return "fail";
        }
    }
}
