package com.gdy.store.controller;

import com.gdy.store.pojo.Address;
import com.gdy.store.pojo.Cart;
import com.gdy.store.pojo.Goods;
import com.gdy.store.service.AddressService;
import com.gdy.store.service.EvaluateService;
import com.gdy.store.service.GoodsService;
import com.gdy.store.service.GoodsTypeService;
import com.gdy.store.pojo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Controller
public class DefaultAddressController {

    @Autowired
    private GoodsService goodsService;
    @Autowired
    private EvaluateService evaluateService;
    @Autowired
    private AddressService addressService;
    @Autowired
    private GoodsTypeService goodsTypeService;

    @RequestMapping("/setDefault/{id}/{goodsId}/{num}")
    public String setDefault(@PathVariable int id, @PathVariable int goodsId, @PathVariable int num, HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        int userId = (int) session.getAttribute("userId");

        addressService.setDefaultAddr(id, userId);
        addressService.setDefaultAddrT(id, userId);

        Goods goods = goodsService.getGoodsById(goodsId);
        List<Address> address = addressService.getAddrByUserId(userId);
        Address address1 = addressService.getAddrByUserIdByDefault(userId);
        BigDecimal price = goods.getGoodsPrice();
        int price2 = price.intValue() * num;
        BigDecimal price3 = new BigDecimal(price2);
        goods.setGoodsPrice(price3);
        model.addAttribute("addressList",address);
        model.addAttribute("address1",address1);
        model.addAttribute("goods",goods);
        model.addAttribute("num",num);

        return "/order";
    }


    @RequestMapping("/setDefault/{id}")
    public String setDefault(@PathVariable int id, HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        int userId = (int) session.getAttribute("userId");

     /*   Address address3 = addressService.selectDefaultAddressByUserId(userId);

        address3.setAddrDefault("否");

        addressService.updateAddress(address3);

        addressService.updateAddressDefault(id,userId);*/

        addressService.setDefaultAddr(id, userId);

        addressService.setDefaultAddrT(id, userId);

        List Classifys = (List) session.getAttribute("Classifys");
        List num = (List) session.getAttribute("num");
        List<Integer> goodsId = new ArrayList();

        BigDecimal Allprice = new BigDecimal("0");

        for(int i =0;i<Classifys.size();i++){
            String  a = (String) Classifys.get(i);
            Integer b = Integer.parseInt(a);
            Cart cart = goodsService.getGoodsIdByCartId(b);
            goodsId.add(cart.getCartGoods());
        }

        List<Goods> goodsList = new ArrayList<>();

        for(int i =0;i<goodsId.size();i++) {
            Integer b = goodsId.get(i);
            Goods goods = goodsService.getGoodsById(b);
            goodsList.add(goods);
        }
        List<Address> address = addressService.getAddrByUserId(userId);
        Address address1 = addressService.getAddrByUserIdByDefault(userId);

        for(int j=0;j<goodsList.size();j++) {
            BigDecimal price = goodsList.get(j).getGoodsPrice();
            String num2 = (String) num.get(j);
            Integer num3 = Integer.parseInt(num2);
            int price2 = price.intValue() * num3;
            BigDecimal price3 = new BigDecimal(price2);
            goodsList.get(j).setGoodsPrice(price3);
            Allprice = Allprice.add(price3);
        }

        for (int c =0;c<num.size();c++){
            String  a = (String) num.get(c);
            Integer b = Integer.parseInt(a);
            goodsList.get(c).setGoodsNums(b);
        }

        model.addAttribute("addressList",address);
        model.addAttribute("address1",address1);
        model.addAttribute("goodsList",goodsList);
        model.addAttribute("num",num);
        model.addAttribute("Allprice",Allprice);

        return "/order2";
    }

}
