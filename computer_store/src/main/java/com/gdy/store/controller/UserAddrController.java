package com.gdy.store.controller;

import com.gdy.store.pojo.*;
import com.gdy.store.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/userAddress")
public class UserAddrController {
    @Autowired
    private AddressService addressService;
    @Autowired
    private ProvinceService provinceService;
    @Autowired
    private CityService cityService;
    @Autowired
    private AreaService areaService;
    @Autowired
    private StreetService streetService;

    /*添加收货地址*/
    @RequestMapping("/addAddress")
    @ResponseBody
    public String addAddress(Address address, HttpServletRequest request){
        HttpSession session =  request.getSession();
        User user = (User) session.getAttribute("user");
        Province province = provinceService.getByProvId(Integer.parseInt(address.getAddrProvince()));
        City city = cityService.getByCityId(Integer.parseInt(address.getAddrCity()));
       /* if ("市辖区".equals(city.getCityName())||"县".equals(city.getCityName())||"市".equals(city.getCityName())){
            address.setAddrCity("");
        }else {
        }*/
        address.setAddrCity(city.getCityName());

        if("none".equals(address.getAddrArea())){
            address.setAddrArea("");
        }else {
            Area area = areaService.getByAreaId(Integer.parseInt(address.getAddrArea()));
            address.setAddrArea(area.getAreaName());
        }
        if("none".equals(address.getAddrStreet())||address.getAddrStreet()==null){
            address.setAddrStreet("");
        }else {
            Street street = streetService.getByStreetId(Integer.parseInt(address.getAddrStreet()));
            address.setAddrStreet(street.getStreetName());
        }
        address.setAddrProvince(province.getProvinceName());
        List<Address> allAddrByUserId = addressService.getAllAddrByUserId(user.getUserId());
        if(allAddrByUserId.isEmpty()){
            address.setAddrDefault("是");
        }else {
            address.setAddrDefault("否");
        }
        int rs = addressService.addAddress(address);
        if (rs>0){
            return "success";
        }else
            return "fail";
    }

    /*根据用户Id获取所有收货地址*/
    @RequestMapping("/getAllAddrByUserId")
    @ResponseBody
    public List<Address> getAllAddrByUserId(HttpServletRequest request) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        List<Address> addrList = addressService.getAllAddrByUserId(user.getUserId());
        return addrList;
    }

    /*删除收货地址*/
    @RequestMapping("/delAddrById/{addrId}")
    @ResponseBody
    public String delAddrById(@PathVariable int addrId) {
        Address address = addressService.getAddrById(addrId);
        if (address.getAddrDefault()=="是"||"是".equals(address.getAddrDefault())){
            return "no";
        }
        int rs = addressService.delAddrById(addrId);
        if (rs > 0) {
            return "success";
        } else {
            return "fail";
        }
    }

    /*根据id获取某地址*/
    @RequestMapping("/getAddrById/{addrId}")
    @ResponseBody
    public Address getAddrById(@PathVariable int addrId){
        Address addr = addressService.getAddrById(addrId);
        /*省*/
        Province province = provinceService.getByProvName(addr.getAddrProvince());
        addr.setAddrProvince(String.valueOf(province.getProvinceId()));
        /*城市*/
        City city = new City();
        city.setCityName(addr.getAddrCity());
        city.setProvinceId(province.getProvinceId());
        City city2 = cityService.getByCityName(city);
        addr.setAddrCity(String.valueOf(city2.getCityId()));

        if (addr.getAddrArea()==null||"".equals(addr.getAddrArea())){
            addr.setAddrArea("");
            Street street = new Street();
            street.setStreetName(addr.getAddrStreet());
            street.setAreaId(city2.getCityId());
            Street street3 = streetService.getByStreetName(street);
            addr.setAddrStreet(String.valueOf(street3.getStreetId()));
        }else {
            Area area = new Area();
            area.setAreaName(addr.getAddrArea());
            area.setCityId(city2.getCityId());
            Area area2 = areaService.getByAreaName(area);
            addr.setAddrArea(String.valueOf(area2.getAreaId()));
            if (addr.getAddrStreet()==null||"".equals(addr.getAddrStreet())){
                addr.setAddrStreet("");
            }else {
                Street street = new Street();
                street.setStreetName(addr.getAddrStreet());
                street.setAreaId(area2.getAreaId());
                Street street2 = streetService.getByStreetName(street);
                addr.setAddrStreet(String.valueOf(street2.getStreetId()));
            }
        }
        return addr;
    }

    /*更新收货地址*/
    @RequestMapping("/updateAddress")
    @ResponseBody
    public String updateAddress(Address address){
        Province province = provinceService.getByProvId(Integer.parseInt(address.getAddrProvince()));
        City city = cityService.getByCityId(Integer.parseInt(address.getAddrCity()));
       /* if ("市辖区".equals(city.getCityName())||"县".equals(city.getCityName())||"市".equals(city.getCityName())){
            address.setAddrCity("");
        }else {
        }*/
        address.setAddrCity(city.getCityName());
        if("none".equals(address.getAddrArea())){
            address.setAddrArea("");
        }else {
            Area area = areaService.getByAreaId(Integer.parseInt(address.getAddrArea()));
            address.setAddrArea(area.getAreaName());
        }
        if("none".equals(address.getAddrStreet())||address.getAddrStreet()==null){
            address.setAddrStreet("");
        }else {
            Street street = streetService.getByStreetId(Integer.parseInt(address.getAddrStreet()));
            address.setAddrStreet(street.getStreetName());
        }
        address.setAddrProvince(province.getProvinceName());
        int rs = addressService.updateAddress(address);
        if (rs>0){
            return "success";
        }else {
            return "fail";
        }
    }

    @RequestMapping("/setDefaultAddr/{addrId}/{userId}")
    @ResponseBody
    public String setDefaultAddr(@PathVariable int addrId, @PathVariable int userId) {
        int rs = addressService.setDefaultAddr(addrId, userId);
        int rs2 = addressService.setDefaultAddrT(addrId, userId);
        if (rs > 0 && rs2 > 0) {
            return "success";
        } else {
            return "fail";
        }
    }
}
