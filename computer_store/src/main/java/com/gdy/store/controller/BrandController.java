package com.gdy.store.controller;

import com.alibaba.fastjson.JSONObject;
import com.gdy.store.service.BrandService;
import com.gdy.store.service.GoodsService;
import com.gdy.store.pojo.Brand;
import com.gdy.store.pojo.Goods;
import com.gdy.store.tools.PageBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/brand")
public class BrandController {
    @Autowired
    private BrandService brandService;
    @Autowired
    private GoodsService goodsService;

    /*管理员获取banner全部信息*/
    @RequestMapping("/getAllBrandByAdmin")
    @ResponseBody
    public JSONObject getAllBrandByAdmin(int page, int limit, String keyWord) {
        PageBean pageBean = new PageBean();
        pageBean.setPage(page);
        pageBean.setLimit(limit);
        PageBean<Brand> pBean = brandService.getPageBean(pageBean.getPage(), pageBean.getLimit(), keyWord);
        JSONObject jsObj = new JSONObject();
        jsObj.put("msg", "");
        jsObj.put("code", 0);
        jsObj.put("count", pBean.getTotalRecs());
        jsObj.put("data", pBean.getJsArr());
        return jsObj;
    }

    @RequestMapping("/updateBrand/{brandId}/{brandName}")
    @ResponseBody
    public String updateBrand(@PathVariable int brandId,@PathVariable String brandName){
        int rs = brandService.updateBrand(brandId, brandName);
        if (rs>0){
            return "success";
        }else {
            return "fail";
        }
    }
    @RequestMapping("deleteBrand/{brandId}")
    @ResponseBody
    public String deleteBrand(@PathVariable int brandId){
        int rs2 =0;
        int rs = brandService.deleteBrand(brandId);
        List<Goods> goodsList = goodsService.getGoodsByBrand(brandId);
        if (goodsList.isEmpty()||goodsList.size()==0){
            rs2=1;
        }else {
           goodsService.deleteGoodsByBrand(brandId);
           rs2=2;
        }
        if (rs>0&&rs2>0){
            return "success";
        }else {
            return "fail";
        }
    }
    @RequestMapping("/addBrand/{brandName}")
    @ResponseBody
    public String addBrand(@PathVariable String brandName){
        int rs = brandService.addBrand(brandName);
        if (rs>0){
            return "success";
        }else {
            return "fail";
        }
    }

}
