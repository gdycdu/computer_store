package com.gdy.store.controller;

import com.alibaba.fastjson.JSONObject;
import com.gdy.store.pojo.Admin;
import com.gdy.store.service.AdminService;
import com.gdy.store.service.UserService;
import com.gdy.store.pojo.User;
import com.gdy.store.tools.MD5Util;
import com.gdy.store.tools.VerifyCodeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("admin")
public class AdminController {
    @Autowired
    private AdminService adminService;
    @Autowired
    private UserService userService;

    //验证码
    @RequestMapping("/verifyCode")
    public void verifyCode(HttpServletResponse response, HttpSession session) throws Exception {
        response.setHeader("Pragma", "No-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        response.setContentType("image/jpeg");
        String verifyCode = VerifyCodeTool.generateVerifyCode(4);
        session.setAttribute("code", verifyCode);
        VerifyCodeTool.outputImage(105, 46, response.getOutputStream(), verifyCode);
    }

    @RequestMapping("/admin_login")
    @ResponseBody
    public String AdminLogin(Admin admin, Model model, HttpServletRequest request, HttpSession session) {
        String code = request.getParameter("vercode");
        String cnt = session.getAttribute("code").toString();
        String adminName = admin.getAdminName();
        if (adminName == null) {
            adminName = admin.getAdminEmail();
        }
        if (adminName == null) {
            adminName = admin.getAdminPhone();
        }
        String em = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
        String ph = "^((13[0-9])|(15[^4,\\D])|(17[0-9])|(18[0,5-9]))\\d{8}$";
        if (adminName.matches(ph)) {
            admin.setAdminName(null);
            admin.setAdminPhone(adminName);
        } else if (adminName.matches(em)) {
            admin.setAdminName(null);
            admin.setAdminEmail(adminName);
        }
        admin.setAdminPwd(MD5Util.PwdMD5(admin.getAdminPwd()));
        Admin admin_login = adminService.adminLogin(admin);
        if (code.equals(cnt)) {
            if (admin_login != null) {
                session.setAttribute("admin", admin_login);
                return "ok";
            } else {
                return "fail";
            }
        } else {
            return "fail_code";
        }
    }
    /*添加商品品牌页面*/
    @RequestMapping("/addBrand")
    public String toAddBrand(){
        return "/admin/addBrand";
    }
    /*商品品牌列表*/
    @RequestMapping("/brandList")
    public String toBrandList(){
        return "/admin/brandList";
    }
    /*用户列表页面*/
    @RequestMapping("/userList")
    public String toUserlist() {
        return "/admin/userList";
    }

    /*用户列表页面*/
    @RequestMapping("/storeList")
    public String toStoreList() {
        return "/admin/storeList";
    }

    /*用户列表页面*/
    @RequestMapping("/store")
    public String toStore() {
        return "/admin/store";
    }

    /*添加用户页面*/
    @RequestMapping("/userAdd")
    public String toUserAdd() {
        return "/admin/admin_addUser";
    }

    /*添加店铺页面*/
    @RequestMapping("/storeAdd")
    public String toStoreAdd(Model model) {

        List<User> userList = userService.selectAllUser();

        model.addAttribute("userList",userList);

        return "/admin/admin_addStore";
    }

    /*添加商品页面*/
    @RequestMapping("/goodAdd")
    public String toGoodAdd(Model model) {


        return "/admin/admin_addGoods";
    }


    /*商品页面*/
    @RequestMapping("/goods")
    public String toGoods() {
        return "/admin/goods";
    }

    /*商品页面*/
    @RequestMapping("/orders")
    public String toOrders() {
        return "/admin/orders";
    }


    /*评论页面*/
    @RequestMapping("/commentary")
    public String toCommentary() {
        return "/admin/commentary";
    }

    /*轮播图页面*/
    @RequestMapping("/bannerList")
    public String toBannerList() {
        return "/admin/bannerList";
    }

    /*轮播图添加页面*/
    @RequestMapping("/bannerAdd")
    public String toBannerAdd() {
        return "/admin/admin_addBanner";
    }

    /*统计页面*/
    @RequestMapping("/store_show")
    public String toTest2(){
        return "/admin/store_show";
    }

    /*管理员修改密码页面*/
    @RequestMapping("/toUpPwd")
    public String toUpPwd() {
        return "/admin/admin_upPwd";
    }

    /*验证密码*/
    @RequestMapping("/verifyPwd/{oldPwd}")
    @ResponseBody
    public String verifyPwd(@PathVariable String oldPwd, HttpServletRequest request) {
        HttpSession session = request.getSession();
        Admin admin = (Admin) session.getAttribute("admin");
        if (MD5Util.PwdMD5(oldPwd).equals(admin.getAdminPwd())) {
            return "success";
        } else {
            return "fail";
        }
    }

    /*修改密码*/
    @RequestMapping("/upAdminPwd/{adminPwd}")
    @ResponseBody
    public String upUserPwd(@PathVariable String adminPwd, HttpServletRequest request) {
        HttpSession session = request.getSession();
        Admin admin = (Admin) session.getAttribute("admin");
        admin.setAdminPwd(MD5Util.PwdMD5(adminPwd));
        int rs = adminService.updatePwd(admin);
        if (rs > 0) {
            return "success";
        } else {
            return "fail";
        }
    }

    //注销 退出登录
    @RequestMapping("/admin_logout")
    @ResponseBody
    public String logout(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.removeAttribute("admin");
        return "success";
    }

    /*通过Id获取admin*/
    @RequestMapping("/getAdminById/{adminId}")
    public String getById(@PathVariable int adminId, Model model) {
        Admin admin = adminService.getAdminById(adminId);
        model.addAttribute("admin", admin);
        return "/admin/admin_update";
    }

    //上传头像
    @RequestMapping(value = "/upload")
    @ResponseBody
    public JSONObject upUserImg(@RequestParam(value = "file") MultipartFile file) throws IOException {
        String name = file.getOriginalFilename();
        String path = "D:/IntelliJ IDEA 2022.3.2/workspace/computer_store/src/main/resources/static/img/header/admin/" + name;
        file.transferTo(new File(path));
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("adminImg", name);
        return jsonObject;
    }

    @RequestMapping("/updateAdmin")
    @ResponseBody
    public String updateAdmin(Admin admin, HttpServletRequest request) {
        int rs = adminService.updateAdmin(admin);
        admin = adminService.getAdminById(admin.getAdminId());
        HttpSession session = request.getSession();
        session.setAttribute("admin", admin);
        if (rs > 0) {
            return "success";
        } else {
            return "fail";
        }
    }
}
