package com.gdy.store.controller;

import com.gdy.store.pojo.Address;
import com.gdy.store.pojo.Cart;
import com.gdy.store.pojo.Goods;
import com.gdy.store.service.AddressService;
import com.gdy.store.service.EvaluateService;
import com.gdy.store.service.GoodsService;
import com.gdy.store.service.GoodsTypeService;
import com.gdy.store.pojo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Controller
public class cartController {

    @Autowired
    private GoodsService goodsService;
    @Autowired
    private EvaluateService evaluateService;
    @Autowired
    private AddressService addressService;
    @Autowired
    private GoodsTypeService goodsTypeService;

    @RequestMapping("/cartGoods/{id}/{price}")
    @ResponseBody
    public String cartGoods(@PathVariable int id, @PathVariable BigDecimal price, Model model, HttpSession session, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int userId = (int) session.getAttribute("userId");

        List<Integer> list = goodsService.selectCartByGoodIdAndPriceAndUserId(id,price,userId);

        if(list!=null && list.size()>0){
            int i = goodsService.updataCartNums(id, price, userId);
           if(i>0){
               return "add";
           }else{
               return "fail";
           }
        }else {
            Cart cart = new Cart();
            cart.setCartGoods(id);
            cart.setCartNum(1);
            cart.setCartPrice(price);
            cart.setCartUser(userId);
            int k = goodsService.insertCart(cart);
            if (k>0){
                return "sucess";
            }else {
                return "fail";
            }
        }
    }

    @RequestMapping("/cartGoodsDelete/{CartId}")
    @ResponseBody
    public String cartGoods(@PathVariable int CartId,Model model, HttpSession session, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int rs = goodsService.deleteCartByCartId(CartId);
        if (rs>0){
            return "success";
        }else {
            return "fail";
        }
    }

    @RequestMapping("/addCartNums/{cartId}/{goodsId}")
    @ResponseBody
    public String addCartNums(@PathVariable int cartId,@PathVariable int goodsId){
        int rs = goodsService.addCartNums(cartId);
        if (rs>0){
            return "success";
        }else {
            return "fail";
        }
    }

    @RequestMapping("/reduceCartNums/{cartId}/{goodsId}")
    @ResponseBody
    public String reduceCartNums(@PathVariable int cartId,@PathVariable int goodsId){
        Cart cart = goodsService.getCartById(cartId);
        if (cart.getCartNum()>1){
            int rs = goodsService.reduceCartNums(cartId);
            if (rs>0){
                return "success";
            }else {
                return "fail";
            }
        }else {
            return "no";
        }
    }
    
    @RequestMapping(value = "/cartGoodsPay/{Classifys}/{num}")
    public String cartGoods(@PathVariable List Classifys,@PathVariable List num,Model model, HttpSession session, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        session.setAttribute("Classifys",Classifys);
        session.setAttribute("num",num);

        List<Integer> goodsId = new ArrayList();
        BigDecimal Allprice = new BigDecimal("0");

        for(int i =0;i<Classifys.size();i++){
            String  a = (String) Classifys.get(i);
            Integer b = Integer.parseInt(a);
            Cart cart = goodsService.getGoodsIdByCartId(b);
            goodsId.add(cart.getCartGoods());
        }

        int userId = (int) session.getAttribute("userId");

        List<Goods> goodsList = new ArrayList<>();

        for(int i =0;i<goodsId.size();i++) {
            Integer b = goodsId.get(i);
            Goods goods = goodsService.getGoodsById(b);
            goodsList.add(goods);
        }
        List<Address> address = addressService.getAddrByUserId(userId);
        Address address1 = addressService.getAddrByUserIdByDefault(userId);
        /*价格*/
        for(int j=0;j<goodsList.size();j++) {
            BigDecimal price = goodsList.get(j).getGoodsPrice();
            String num2 = (String) num.get(j);
            Integer num3 = Integer.parseInt(num2);
            int price2 = price.intValue() * num3;
            BigDecimal price3 = new BigDecimal(price2);
            goodsList.get(j).setGoodsPrice(price3);
            Allprice = Allprice.add(price3);
        }

        // 数量
        for (int c =0;c<num.size();c++){
            String  a = (String) num.get(c);
            Integer b = Integer.parseInt(a);
            goodsList.get(c).setGoodsNums(b);
        }

        model.addAttribute("addressList",address);
        model.addAttribute("address1",address1);
        model.addAttribute("goodsList",goodsList);
        model.addAttribute("num",num);
        model.addAttribute("Allprice",Allprice);

        return "/order2";
    }


}
