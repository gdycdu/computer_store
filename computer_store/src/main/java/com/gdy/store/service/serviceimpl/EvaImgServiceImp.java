package com.gdy.store.service.serviceimpl;

import com.gdy.store.service.EvaImgService;
import com.gdy.store.mapper.EvaimgMapper;
import com.gdy.store.pojo.Evaimg;
import com.gdy.store.pojo.EvaimgExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EvaImgServiceImp implements EvaImgService {
    @Autowired
    private EvaimgMapper evaImgMapper;

    @Override
    public int addEvaImg(Evaimg evaImg) {
        return evaImgMapper.insertSelective(evaImg);
    }

    @Override
    public List<Evaimg> getAllEvaImgByEvaId(int evaluate) {
        EvaimgExample example = new EvaimgExample();
        EvaimgExample.Criteria criteria = example.createCriteria();
        criteria.andEvaluateEqualTo(evaluate);
        return evaImgMapper.selectByExample(example);
    }

    @Override
    public Evaimg getEvaImgById(int evaImgId) {

        return evaImgMapper.selectByPrimaryKey(evaImgId);
    }
}
