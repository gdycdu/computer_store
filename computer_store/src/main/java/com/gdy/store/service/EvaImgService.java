package com.gdy.store.service;

import com.gdy.store.pojo.Evaimg;

import java.util.List;

public interface EvaImgService {
    int addEvaImg(Evaimg evaImg);

    List<Evaimg> getAllEvaImgByEvaId(int evaluate);

    Evaimg getEvaImgById(int evaImgId);
}
