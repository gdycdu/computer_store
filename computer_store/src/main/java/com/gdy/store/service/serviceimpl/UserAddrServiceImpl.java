package com.gdy.store.service.serviceimpl;

import com.gdy.store.service.AddressService;
import com.gdy.store.mapper.AddressMapper;
import com.gdy.store.pojo.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserAddrServiceImpl implements AddressService {
    @Autowired
    private AddressMapper addressMapper;
    private int addrUser;

    @Override
    public int addAddress(Address address) {
        return addressMapper.addAddress(address);
    }

    @Override
    public List<Address> getAllAddrByUserId(int addrUser) {
        return addressMapper.getAllAddrByUserId(addrUser);
    }

    @Override
    public List<Address> getAddrByUserId(int UserId) {
        return addressMapper.selectByUserId(UserId);
    }

    @Override
    public Address getAddrByUserIdByDefault(int UserId) {
        return addressMapper.selectByUserIdByDefault(UserId);
    }


    @Override
    public int delAddrById(int addrId) {
        return addressMapper.delAddrById(addrId);
    }

    @Override
    public Address getAddrById(int addrId) {

        return addressMapper.getAddrById(addrId);
    }

    @Override
    public int updateAddress(Address address) {
        return addressMapper.updateAddress(address);
    }

    @Override
    public int updateByPrimaryKeySelective(Address address) {
        return addressMapper.updateByPrimaryKeySelective(address);
    }

    @Override
    public int setDefaultAddr(int addrId, int userId) {

        return addressMapper.setDefaultAddr(addrId, userId);
    }

    @Override
    public int setDefaultAddrT(int addrId, int userId) {

        return addressMapper.setDefaultAddrT(addrId, userId);
    }

    @Override
    public Address selectDefaultAddress(int addrId) {
        return addressMapper.selectDefaultAddress(addrId);
    }

    @Override
    public Address selectDefaultAddressByUserId(int userId) {
        return addressMapper.selectDefaultAddressByUserId(userId);
    }

    @Override
    public int updateAddressDefault(int addrId,int userId) {
        return addressMapper.updateAddressDefault(addrId,userId);
    }

}
