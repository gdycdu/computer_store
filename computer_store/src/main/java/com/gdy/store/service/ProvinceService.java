package com.gdy.store.service;

import com.gdy.store.pojo.Province;

import java.util.List;

public interface ProvinceService {
    List<Province> getAllProv();

    Province getByProvId(int provinceId);

    Province getByProvName(String provinceName);
}
