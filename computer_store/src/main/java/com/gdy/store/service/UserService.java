package com.gdy.store.service;

import com.gdy.store.pojo.User;
import com.gdy.store.tools.PageBean;

import java.util.List;

public interface UserService {
    List<User> getAllUsers(int offset, int size);//
    PageBean<User> getPageBean(int page, int size, String keyWord);//
    int deleteUser(int userId);//
    int changeUserState(int userState,int userId);
    User userLogin(User user);
    int addUser(User user);
    int updateUser(User user);
    int updatePwd(User user);
    /*=============*/
    User getByName(String userName);
    User getByPhone(String userPhone);
    User getByEmail(String userEmail);
    User getById(int id);
    /*==============*/
    User getByName2(String userName, int userId);
    User getByPhone2(String userName, int userId);
    User getByEmail2(String userName, int userId);

    List<User> selectAllUser();

    User selectUserByUserId(int userId);

    int updataUserRole(String userName);

}
