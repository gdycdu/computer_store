package com.gdy.store.service.serviceimpl;

import com.gdy.store.mapper.*;
import com.gdy.store.pojo.*;
import com.gdy.store.service.EvaluateService;
import com.gdy.store.tools.PageBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Service
public class EvaluateServiceImp implements EvaluateService {
    @Autowired
    private EvaluateMapper evaluateMapper;
    @Autowired
    private EvaimgMapper evaImgMapper;
    @Autowired
    private StoreMapper storeMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private GoodsMapper goodsMapper;

    @Override
    public List<Evaluate> getEvaByGoodsId(int evaGoods) {
        EvaluateExample example = new EvaluateExample();
        EvaluateExample.Criteria criteria = example.createCriteria();
        criteria.andEvaGoodsEqualTo(evaGoods);
        return evaluateMapper.selectByExample2(example);
    }

    @Override
    public List<Evaluate> getAllEva() {
        EvaluateExample example = new EvaluateExample();
        EvaluateExample.Criteria criteria = example.createCriteria();
        criteria.andEvaStateEqualTo(1);
        return evaluateMapper.selectByExample2(example);
    }

    @Override
    public int addEvaluate(Evaluate evaluate, String[] imgs) {
        int rs = evaluateMapper.insertSelective(evaluate);
        if (rs > 0) {
            for (String img : imgs) {
                Evaimg evaImg = new Evaimg();
                evaImg.setEvaimgName(img);
                evaImg.setEvaluate(evaluate.getEvaId());
                evaImgMapper.insertSelective(evaImg);
            }
        }
        return rs;
    }


    @Override
    public PageBean<Evaluate2> getPageBean(int page, int size, String keyWord, int userId) {
        Store store =  storeMapper.selectByUserId(userId);
        List<Evaluate> evaluateList = new ArrayList<>();
        int cnt = evaluateMapper.getUsersCount(store.getStoreId());
        int offset = (page <= 0) ? 0 : (page - 1) * size;
        int storeId = store.getStoreId();
        if (keyWord == null) {
            evaluateList = evaluateMapper.getAllUsers(offset, size,storeId);
        }
        List<Evaluate2> evaluateList2 = new ArrayList<>();
        SimpleDateFormat sf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (Evaluate evaluate : evaluateList){
            Evaluate2 evaluate2 = new Evaluate2();
            User user = userMapper.selectByPrimaryKey(evaluate.getEvaUser());
            Goods goods = goodsMapper.selectByPrimaryKey(evaluate.getEvaGoods());
            evaluate2.setEvaId(evaluate.getEvaId());
            evaluate2.setEvaUser(user.getUserName());
            evaluate2.setEvaGoods(goods.getGoodsName());
            evaluate2.setGoodsImg(goods.getGoodsImg());
            evaluate2.setGoodsId(goods.getGoodsId());
            evaluate2.setEvaContent(evaluate.getEvaContent());
            evaluate2.setEvaLevel(evaluate.getEvaLevel());
            evaluate2.setEvaDate(evaluate.getEvaDate());
            evaluateList2.add(evaluate2);
        }

        PageBean<Evaluate2> pgBean = new PageBean<Evaluate2>();
        pgBean.setTotalRecs(cnt); //{ps} 设置总记录数
        pgBean.setList(evaluateList2);     //{ps} 设置查出来的结果
        return pgBean;  //返回一个 PageBean
    }

    @Override
    public PageBean<Evaluate2> getPageBean2(int page, int size, int search, int userId) {
        Store store =  storeMapper.selectByUserId(userId);
        List<Evaluate> evaluateList = new ArrayList<>();
        int cnt = evaluateMapper.getCount(store.getStoreId(),search);
        int offset = (page <= 0) ? 0 : (page - 1) * size;
        int storeId = store.getStoreId();
        evaluateList = evaluateMapper.getByGoodsId(offset, size,storeId,search);
        List<Evaluate2> evaluateList2 = new ArrayList<>();
        for (Evaluate evaluate : evaluateList){
            Evaluate2 evaluate2 = new Evaluate2();
            User user = userMapper.selectByPrimaryKey(evaluate.getEvaUser());
            Goods goods = goodsMapper.selectByPrimaryKey(evaluate.getEvaGoods());
            evaluate2.setEvaId(evaluate.getEvaId());
            evaluate2.setEvaUser(user.getUserName());
            evaluate2.setEvaGoods(goods.getGoodsName());
            evaluate2.setGoodsId(goods.getGoodsId());
            evaluate2.setGoodsImg(goods.getGoodsImg());
            evaluate2.setEvaContent(evaluate.getEvaContent());
            evaluate2.setEvaLevel(evaluate.getEvaLevel());
            evaluate2.setEvaDate(evaluate.getEvaDate());
            evaluateList2.add(evaluate2);
        }
        PageBean<Evaluate2> pgBean = new PageBean<Evaluate2>();
        pgBean.setTotalRecs(cnt); //{ps} 设置总记录数
        pgBean.setList(evaluateList2);     //{ps} 设置查出来的结果
        return pgBean;  //返回一个 PageBean
    }


    @Override
    public int insertEvaluate(Evaluate evaluate) {
        return evaluateMapper.insertSelective(evaluate);
    }

    @Override
    public List<Evaluate2> getAllEva2(int userId) {
        Store store =  storeMapper.selectByUserId(userId);
        List<Evaluate> evaluateList = evaluateMapper.getAllEva(store.getStoreId());
        List<Evaluate2> evaluateList2 = new ArrayList<>();
        for (Evaluate evaluate : evaluateList){
            Evaluate2 evaluate2 = new Evaluate2();
            User user = userMapper.selectByPrimaryKey(evaluate.getEvaUser());
            Goods goods = goodsMapper.selectByPrimaryKey(evaluate.getEvaGoods());
            evaluate2.setEvaId(evaluate.getEvaId());
            evaluate2.setEvaUser(user.getUserName());
            evaluate2.setEvaGoods(goods.getGoodsName());
            evaluate2.setGoodsId(goods.getGoodsId());
            evaluate2.setGoodsImg(goods.getGoodsImg());
            evaluate2.setEvaContent(evaluate.getEvaContent());
            evaluate2.setEvaLevel(evaluate.getEvaLevel());
            evaluate2.setEvaDate(evaluate.getEvaDate());
            evaluateList2.add(evaluate2);
        }
        return evaluateList2;
    }

    @Override
    public int getCountWhereF(int storeId) {
        return evaluateMapper.getCountWhereF(storeId);
    }

    @Override
    public int getCountWhereO(int storeId) {
        return evaluateMapper.getCountWhereO(storeId);
    }

}
