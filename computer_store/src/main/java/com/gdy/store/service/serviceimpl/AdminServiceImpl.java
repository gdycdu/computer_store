package com.gdy.store.service.serviceimpl;

import com.gdy.store.service.AdminService;
import com.gdy.store.mapper.AdminMapper;
import com.gdy.store.pojo.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    private AdminMapper adminMapper;

    @Override
    public Admin adminLogin(Admin admin) {
        return adminMapper.adminLogin(admin);
    }

    @Override
    public int updatePwd(Admin admin) {

        return adminMapper.updatePwd(admin);
    }

    @Override
    public Admin getAdminById(int adminId) {
        return adminMapper.getAdminById(adminId);
    }

    @Override
    public int updateAdmin(Admin admin) {
        return adminMapper.updateAdmin(admin);
    }
}
