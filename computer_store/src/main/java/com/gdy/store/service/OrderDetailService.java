package com.gdy.store.service;

import com.gdy.store.pojo.OrderDetail;

import java.util.List;

public interface OrderDetailService {
    OrderDetail getOrderDeteilByOrderId(Long OrderId);

    List<OrderDetail> getByGoodsId(int goodsId);

    int deleteByGoods(int goodsId);


}
