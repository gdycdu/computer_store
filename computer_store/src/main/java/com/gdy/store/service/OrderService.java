package com.gdy.store.service;

import com.gdy.store.pojo.OrderDetail;
import com.gdy.store.pojo.Orderinfo;
import com.gdy.store.pojo.orderGoods2;

import com.gdy.store.tools.PageBean;

import java.util.List;

public interface OrderService {
    void insertOrderByuserId(Orderinfo order);

    List<Orderinfo> selectOrderByUserId(int userId);

    Orderinfo selectOrderByOrderId(Long OrderId);

    int insertOrder(Orderinfo order);

    int updataByno(Orderinfo order);

    int insertDetail(OrderDetail orderDetail);

    OrderDetail selectGoodsIdByOrderId(Long OrderId);

    int deleteByorderId(Long orderId);

    int deleteDetailByorderId(Long orderId);

    PageBean<orderGoods2> getPageBean(int page, int size, String keyWord, int userId);//

    PageBean<orderGoods2> getPageBean2(int page, int size, String keyWord, String search,int userId);//

    int getOrderCountByStore(int storeId);

    int getOrderCountWaitDeliver(int storeId);

    int getUserCountByOrder(int storeId);

    List<Orderinfo> getOrderByStoreId(int storeId);

}
