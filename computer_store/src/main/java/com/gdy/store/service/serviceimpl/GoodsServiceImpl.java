package com.gdy.store.service.serviceimpl;

import com.gdy.store.mapper.*;
import com.gdy.store.pojo.*;
import com.gdy.store.service.GoodsService;
import com.gdy.store.tools.PageBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class GoodsServiceImpl implements GoodsService {
    @Autowired
    GoodsMapper goodsMapper;
    @Autowired
    CartMapper cartMapper;
    @Autowired
    StoreMapper storeMapper;
    @Autowired
    GoodsTypeMapper goodsTypeMapper;
    @Autowired
    BrandMapper brandMapper;
    @Autowired
    OrderinfoMapper orderinfoMapper;
    @Autowired
    com.gdy.store.mapper.OrderDetailMapper OrderDetailMapper;

    @Override
    public List<Goods> getAllGoods() {
        return goodsMapper.selectAllGoods();
    }

    @Override
    public Goods getGoodsById(int goodsId) {
        return goodsMapper.selectByPrimaryKey(goodsId);
    }

    @Override
    public List<Cart> getCartByUserId(int userId) {
        return cartMapper.selectCartByUserId(userId);
    }

    @Override
    public int insertCart(Cart cart) {
        return cartMapper.insertSelective(cart);
    }

    @Override
    public int deleteCartByCartId(int CartId) {
        return cartMapper.deleteByPrimaryKey(CartId);
    }

    @Override
    public Cart getGoodsIdByCartId(int CartId) {
        return cartMapper.selectByPrimaryKey(CartId);
    }

    @Override
    public int addCartNums(int cartId) {
        return cartMapper.addCartNums(cartId);
    }

    @Override
    public int reduceCartNums(int cartId) {
        return cartMapper.reduceCartNums(cartId);
    }

    @Override
    public Cart getCartById(int cartId) {
        return cartMapper.getCartById(cartId);
    }

    @Override
    public PageBean<GoodsIntact> getPageBean(int page, int size, String keyWord, int userId) {
        Store store =  storeMapper.selectByUserId(userId);
        List<Goods> goodsList = new ArrayList<>();
        int cnt = goodsMapper.getUsersCount(store.getStoreId());
        int offset = (page <= 0) ? 0 : (page - 1) * size;
        int storeId = store.getStoreId();
        if (keyWord == null) {
            goodsList = goodsMapper.getAllUsers(offset, size,storeId);
        }

        List<GoodsIntact> goodsIntactList = new ArrayList<>();

        for (int i =0;i < goodsList.size();i++){
            GoodsIntact goodsIntact = new GoodsIntact();
            GoodsType goodsType = goodsTypeMapper.selectTypeByTypeId(goodsList.get(i).getGoodsType());
            Brand brand = brandMapper.selectByPrimaryKey(goodsList.get(i).getGoodsBrand());
            goodsIntact.setGoodsId(goodsList.get(i).getGoodsId());
            goodsIntact.setGoodsName(goodsList.get(i).getGoodsName());
            goodsIntact.setGoodsNums(goodsList.get(i).getGoodsNums());
            goodsIntact.setGoodsBrand(goodsList.get(i).getGoodsBrand());
            goodsIntact.setGoodsType(goodsList.get(i).getGoodsType());
            goodsIntact.setGoodsPrice(goodsList.get(i).getGoodsPrice());
            goodsIntact.setGoodsAttr(goodsList.get(i).getGoodsAttr());
            goodsIntact.setGoodsDesc(goodsList.get(i).getGoodsDesc());
            goodsIntact.setGoodsSales(goodsList.get(i).getGoodsSales());
            goodsIntact.setGoodsState(goodsList.get(i).getGoodsState());
            goodsIntact.setGoodsImg(goodsList.get(i).getGoodsImg());
            goodsIntact.setGoodsStore(goodsList.get(i).getGoodsStore());
            goodsIntact.setCreateTime(goodsList.get(i).getCreateTime());
            goodsIntact.setUpdateTime(goodsList.get(i).getUpdateTime());
            goodsIntact.setTypeId(goodsType.getTypeId());
            goodsIntact.setTypeName(goodsType.getTypeName());
            goodsIntact.setTypeState(goodsType.getTypeState());
            goodsIntact.setBrandId(brand.getBrandId());
            goodsIntact.setBrandName(brand.getBrandName());
            goodsIntactList.add(goodsIntact);
        }

        PageBean<GoodsIntact> pgBean = new PageBean<GoodsIntact>();
        pgBean.setTotalRecs(cnt); //{ps} 设置总记录数
        pgBean.setList(goodsIntactList);     //{ps} 设置查出来的结果
        return pgBean;  //返回一个 PageBean
    }


    @Override
    public PageBean<GoodsIntact> getPageBean2(int page, int size, String keyWord,String search,int userId) {
        Store store =  storeMapper.selectByUserId(userId);
        List<Goods> goodsList = new ArrayList<>();
        int cnt = goodsMapper.getUsersCount2(store.getStoreId(),search);
        int offset = (page <= 0) ? 0 : (page - 1) * size;
        int storeId = store.getStoreId();
        if (keyWord == null) {
            goodsList = goodsMapper.getAllUsers2(offset, size,storeId,search);
        }
        List<GoodsIntact> goodsIntactList = new ArrayList<>();
        for (int i =0;i < goodsList.size();i++){
            GoodsIntact goodsIntact = new GoodsIntact();
            GoodsType goodsType = goodsTypeMapper.selectTypeByTypeId(goodsList.get(i).getGoodsType());
            Brand brand = brandMapper.selectByPrimaryKey(goodsList.get(i).getGoodsBrand());
            goodsIntact.setGoodsId(goodsList.get(i).getGoodsId());
            goodsIntact.setGoodsName(goodsList.get(i).getGoodsName());
            goodsIntact.setGoodsNums(goodsList.get(i).getGoodsNums());
            goodsIntact.setGoodsBrand(goodsList.get(i).getGoodsBrand());
            goodsIntact.setGoodsType(goodsList.get(i).getGoodsType());
            goodsIntact.setGoodsPrice(goodsList.get(i).getGoodsPrice());
            goodsIntact.setGoodsAttr(goodsList.get(i).getGoodsAttr());
            goodsIntact.setGoodsDesc(goodsList.get(i).getGoodsDesc());
            goodsIntact.setGoodsSales(goodsList.get(i).getGoodsSales());
            goodsIntact.setGoodsState(goodsList.get(i).getGoodsState());
            goodsIntact.setGoodsImg(goodsList.get(i).getGoodsImg());
            goodsIntact.setGoodsStore(goodsList.get(i).getGoodsStore());
            goodsIntact.setCreateTime(goodsList.get(i).getCreateTime());
            goodsIntact.setUpdateTime(goodsList.get(i).getUpdateTime());
            goodsIntact.setTypeId(goodsType.getTypeId());
            goodsIntact.setTypeName(goodsType.getTypeName());
            goodsIntact.setTypeState(goodsType.getTypeState());
            goodsIntact.setBrandId(brand.getBrandId());
            goodsIntact.setBrandName(brand.getBrandName());
            goodsIntactList.add(goodsIntact);
        }

        PageBean<GoodsIntact> pgBean = new PageBean<GoodsIntact>();
        pgBean.setTotalRecs(cnt); //{ps} 设置总记录数
        pgBean.setList(goodsIntactList);     //{ps} 设置查出来的结果
        return pgBean;  //返回一个 PageBean
    }



    @Override
    public int updateGoods(Goods goods) {
        return goodsMapper.updateGoods(goods);
    }

    @Override
    public int deleteGoodsByGoodsId(int goodsId) {
        return goodsMapper.deleteGoods(goodsId);
    }

    @Override
    public int renewGoods(int goodsId) {
        return goodsMapper.renewGoods(goodsId);
    }

    @Override
    public int updateGoodsByDelivery(Long id) {
        return orderinfoMapper.updateGoodsByDelivery(id);
    }

    @Override
    public int getGoodsByIdReceiving(Long id) {
        return orderinfoMapper.getGoodsByIdReceiving(id);
    }

    @Override
    public int getGoodsByOK(Long id) {
        return orderinfoMapper.getGoodsByOK(id);
    }

    @Override
    public List<Integer> selectCartByGoodIdAndPriceAndUserId(int goodId, BigDecimal price, int userId) {
        return cartMapper.selectCartByGoodIdAndPriceAndUserId(goodId,price,userId);
    }

    @Override
    public int updataCartNums(int goodId, BigDecimal price, int userId) {
        return cartMapper.updataCartNums(goodId,price,userId);
    }

    @Override
    public OrderDetail selectGoodIdByOrderId(Long OrderId) {
        return OrderDetailMapper.selectGoodsIdByOrderId(OrderId);
    }

    @Override
    public int updataGoodsNumsAndSales(int goodsId, int goodNums) {
        return goodsMapper.updataGoodsNumsAndSales(goodsId,goodNums);
    }

    @Override
    public Goods selectStoreByGoodsId(int goodsId) {
        return goodsMapper.selectStoreByGoodsId(goodsId);
    }

    @Override
    public List<Goods> getGoodsByType(int typeId) {
        return goodsMapper.getGoodsByType(typeId);
    }

    @Override
    public List<Goods> getGoodsByBrand(int brandId) {
        return goodsMapper.getGoodsByBrand(brandId);
    }

    @Override
    public int addGoods(Goods goods) {
        return goodsMapper.addGoods(goods);
    }

    @Override
    public Goods getById(int goodsId) {
        return getGoodsById(goodsId);
    }

    @Override
    public List<Goods> getGoodsByStoreId(int storeId) {
        return goodsMapper.getGoodsStoreId(storeId);
    }

    @Override
    public List<Goods> getGoodsStoreId2(int StoreId) {
        return goodsMapper.getGoodsStoreId2(StoreId);
    }

    @Override
    public int getGoodsCountByStore(int storeId) {
        return goodsMapper.getGoodsCountByStore(storeId);
    }

    @Override
    public int deleteGoodsByBrand(int brandId) {
        return goodsMapper.deleteGoodsByBrand(brandId);
    }

    @Override
    public int deleteGoodsByStoreId(int storeId) {
        return goodsMapper.deleteGoodsByStoreId(storeId);
    }

    @Override
    public int renewGoodsByStoreId(int storeId) {
        return goodsMapper.renewGoodsByStoreId(storeId);
    }

    @Override
    public List<Goods> getGoodsByLikeName(String goodsName) {
        return goodsMapper.getGoodsByLikeName(goodsName);
    }

    @Override
    public int deleteCartByGoods(int goodsId) {
        return cartMapper.deleteCartByGoods(goodsId);
    }

    @Override
    public List<Cart> getCartByGoods(int goodsId) {
        return cartMapper.getCartByGoods(goodsId);
    }


}
