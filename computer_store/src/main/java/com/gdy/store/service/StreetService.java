package com.gdy.store.service;

import com.gdy.store.pojo.Street;

import java.util.List;

public interface StreetService {
    List<Street> getStreetByAreaId(int areaId);

    List<Street> getStreetByCityId(int cityId);

    Street getByStreetId(int streetId);

    Street getByStreetName(Street street);
}
