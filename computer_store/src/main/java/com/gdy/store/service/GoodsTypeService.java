package com.gdy.store.service;

import com.gdy.store.pojo.GoodsType;

import java.util.List;

public interface GoodsTypeService {
    List<GoodsType> getByTypeId(int typeId);

    GoodsType getTypeByTypeId(int typeId);

    List<GoodsType> getAllGoodsType();
}
