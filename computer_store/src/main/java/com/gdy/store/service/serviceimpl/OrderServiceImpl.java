package com.gdy.store.service.serviceimpl;

import com.gdy.store.pojo.*;
import com.gdy.store.service.OrderService;
import com.gdy.store.mapper.GoodsMapper;
import com.gdy.store.mapper.OrderDetailMapper;
import com.gdy.store.mapper.OrderinfoMapper;
import com.gdy.store.mapper.StoreMapper;
import com.gdy.store.tools.PageBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderinfoMapper orderinfoMapper;

    @Autowired
    private OrderDetailMapper orderDetailMapper;
    @Autowired
    private StoreMapper storeMapper;
    @Autowired
    private GoodsMapper goodsMapper;

    @Override
    public void insertOrderByuserId(Orderinfo order) {
        orderinfoMapper.insertSelective(order);
    }

    @Override
    public List<Orderinfo> selectOrderByUserId(int userId) {
        return orderinfoMapper.selectByUserId(userId);
    }

    @Override
    public Orderinfo selectOrderByOrderId(Long OrderId) {
        return orderinfoMapper.selectByPrimaryKey(OrderId);
    }

    @Override
    public int insertOrder(Orderinfo order) {
        return orderinfoMapper.insertSelective(order);
    }

    @Override
    public int updataByno(Orderinfo order) {
        return orderinfoMapper.updateByPrimaryKeySelective(order);
    }

    @Override
    public int insertDetail(OrderDetail orderDetail) {
        return orderDetailMapper.insertSelective(orderDetail);
    }

    @Override
    public  OrderDetail selectGoodsIdByOrderId(Long OrderId) {
        return orderDetailMapper.selectGoodsIdByOrderId(OrderId);
    }

    @Override
    public int deleteByorderId(Long orderId) {
        return orderinfoMapper.deleteByPrimaryKey(orderId);
    }

    @Override
    public int deleteDetailByorderId(Long orderId) {
        return orderDetailMapper.deleteByOrderId(orderId);
    }


    @Override
    public PageBean<orderGoods2> getPageBean(int page, int size, String keyWord, int userId) {
        Store store =  storeMapper.selectByUserId(userId);
        List<Orderinfo> orderinfoList = new ArrayList<>();
        int cnt = orderinfoMapper.getUsersCount(store.getStoreId());
        int offset = (page <= 0) ? 0 : (page - 1) * size;
        int storeId = store.getStoreId();
        if (keyWord == null) {
            orderinfoList = orderinfoMapper.getAllUsers(offset, size,storeId);
        } else {
            UserExample example = new UserExample();
            UserExample.Criteria criteria = example.createCriteria();
            criteria.andUserNameLike("%" + keyWord + "%");
            orderinfoList = orderinfoMapper.selectByExample(example);
        }


        List<Integer> orderDetails2 = new ArrayList<>();

        for (Orderinfo orderinfo1 : orderinfoList ) {
            Long id = orderinfo1.getOrderId();
            OrderDetail orderDetail3 = orderDetailMapper.selectGoodsIdByOrderId(id);
            orderDetails2.add(orderDetail3.getDetailGoods());
        }


        List<Goods> goods = new ArrayList<>();

        for (Integer integer : orderDetails2) {
            Goods goods1 = goodsMapper.selectByPrimaryKey(integer);
            goods.add(goods1);
        }

        List<orderGoods2> orderGoodsList = new ArrayList<>();

        for (int i = 0;i < orderinfoList.size();i++) {
            orderGoods2 orderGoods = new orderGoods2();
            String orderId = String.valueOf(orderinfoList.get(i).getOrderId());
            orderGoods.setOrderId(orderId);
            orderGoods.setOrderUser(orderinfoList.get(i).getOrderUser());
            orderGoods.setOrderDate(orderinfoList.get(i).getOrderDate());
            orderGoods.setOrderPrice(orderinfoList.get(i).getOrderPrice());
            orderGoods.setOrderState(orderinfoList.get(i).getOrderState());
            orderGoods.setOrderReceiver(orderinfoList.get(i).getOrderReceiver());
            orderGoods.setOrderPhone(orderinfoList.get(i).getOrderPhone());
            orderGoods.setOrderAddress(orderinfoList.get(i).getOrderAddress());
            orderGoods.setCreateTimeOrder(orderinfoList.get(i).getCreateTime());
            orderGoods.setUpdateTimeOrder(orderinfoList.get(i).getUpdateTime());

            orderGoods.setGoodsId(goods.get(i).getGoodsId());
            orderGoods.setGoodsName(goods.get(i).getGoodsName());
            orderGoods.setGoodsNums(goods.get(i).getGoodsNums());
            orderGoods.setGoodsBrand(goods.get(i).getGoodsBrand());
            orderGoods.setGoodsType(goods.get(i).getGoodsType());
            orderGoods.setGoodsPrice(goods.get(i).getGoodsPrice());
            orderGoods.setGoodsDesc(goods.get(i).getGoodsDesc());
            orderGoods.setGoodsSales(goods.get(i).getGoodsSales());
            orderGoods.setGoodsState(goods.get(i).getGoodsState());
            orderGoods.setGoodsImg(goods.get(i).getGoodsImg());
            orderGoods.setCreateTimeGoods(goods.get(i).getCreateTime());
            orderGoods.setUpdateTimeGoods(goods.get(i).getUpdateTime());

            orderGoodsList.add(orderGoods);
        }




        PageBean<orderGoods2> pgBean = new PageBean<orderGoods2>();
        pgBean.setTotalRecs(cnt); //{ps} 设置总记录数
        pgBean.setList(orderGoodsList);     //{ps} 设置查出来的结果
        return pgBean;  //返回一个 PageBean
    }


    @Override
    public PageBean<orderGoods2> getPageBean2(int page, int size, String keyWord,String search, int userId) {
        Store store =  storeMapper.selectByUserId(userId);
        List<Orderinfo> orderinfoList = new ArrayList<>();
        int cnt = orderinfoMapper.getUsersCount2(store.getStoreId(),search);
        int offset = (page <= 0) ? 0 : (page - 1) * size;
        int storeId = store.getStoreId();
        if (keyWord == null) {
            orderinfoList = orderinfoMapper.getAllUsers2(offset, size,storeId,search);
        } else {
            UserExample example = new UserExample();
            UserExample.Criteria criteria = example.createCriteria();
            criteria.andUserNameLike("%" + keyWord + "%");
            orderinfoList = orderinfoMapper.selectByExample(example);
        }


        List<Integer> orderDetails2 = new ArrayList<>();

        for (Orderinfo orderinfo1 : orderinfoList ) {
            Long id = orderinfo1.getOrderId();
            OrderDetail orderDetail3 = orderDetailMapper.selectGoodsIdByOrderId(id);
            orderDetails2.add(orderDetail3.getDetailGoods());
        }


        List<Goods> goods = new ArrayList<>();

        for (Integer integer : orderDetails2) {
            Goods goods1 = goodsMapper.selectByPrimaryKey(integer);
            goods.add(goods1);
        }

        List<orderGoods2> orderGoodsList = new ArrayList<>();

        for (int i = 0;i < orderinfoList.size();i++) {
            orderGoods2 orderGoods = new orderGoods2();
            String orderId = String.valueOf(orderinfoList.get(i).getOrderId());
            orderGoods.setOrderId(orderId);
            orderGoods.setOrderUser(orderinfoList.get(i).getOrderUser());
            orderGoods.setOrderDate(orderinfoList.get(i).getOrderDate());
            orderGoods.setOrderPrice(orderinfoList.get(i).getOrderPrice());
            orderGoods.setOrderState(orderinfoList.get(i).getOrderState());
            orderGoods.setOrderReceiver(orderinfoList.get(i).getOrderReceiver());
            orderGoods.setOrderPhone(orderinfoList.get(i).getOrderPhone());
            orderGoods.setOrderAddress(orderinfoList.get(i).getOrderAddress());
            orderGoods.setCreateTimeOrder(orderinfoList.get(i).getCreateTime());
            orderGoods.setUpdateTimeOrder(orderinfoList.get(i).getUpdateTime());

            orderGoods.setGoodsId(goods.get(i).getGoodsId());
            orderGoods.setGoodsName(goods.get(i).getGoodsName());
            orderGoods.setGoodsNums(goods.get(i).getGoodsNums());
            orderGoods.setGoodsBrand(goods.get(i).getGoodsBrand());
            orderGoods.setGoodsType(goods.get(i).getGoodsType());
            orderGoods.setGoodsPrice(goods.get(i).getGoodsPrice());
            orderGoods.setGoodsDesc(goods.get(i).getGoodsDesc());
            orderGoods.setGoodsSales(goods.get(i).getGoodsSales());
            orderGoods.setGoodsState(goods.get(i).getGoodsState());
            orderGoods.setGoodsImg(goods.get(i).getGoodsImg());
            orderGoods.setCreateTimeGoods(goods.get(i).getCreateTime());
            orderGoods.setUpdateTimeGoods(goods.get(i).getUpdateTime());

            orderGoodsList.add(orderGoods);
        }




        PageBean<orderGoods2> pgBean = new PageBean<orderGoods2>();
        pgBean.setTotalRecs(cnt); //{ps} 设置总记录数
        pgBean.setList(orderGoodsList);     //{ps} 设置查出来的结果
        return pgBean;  //返回一个 PageBean
    }

    @Override
    public int getOrderCountByStore(int storeId) {
        return orderinfoMapper.getOrderCountByStore(storeId);
    }

    @Override
    public int getOrderCountWaitDeliver(int storeId) {
        return orderinfoMapper.getOrderCountWaitDeliver(storeId);
    }

    @Override
    public int getUserCountByOrder(int storeId) {
        return orderinfoMapper.getUserCountByOrder(storeId);
    }

    @Override
    public List<Orderinfo> getOrderByStoreId(int storeId) {
        return orderinfoMapper.getOrderByStoreId(storeId);
    }

}
