package com.gdy.store.service;

import com.gdy.store.pojo.Brand;
import com.gdy.store.tools.PageBean;

import java.util.List;

public interface BrandService {
    Brand getByBrandId(int brandId);

    List<Brand> getAllBrand();

    PageBean<Brand> getPageBean(int page, int size, String keyWord);

    int getBrandCount();

    List<Brand> getBrandyLikeName(String brandName);

    int updateBrand(int brandId,String brandName);

    int deleteBrand(int brandId);

    int addBrand(String brandName);
}
