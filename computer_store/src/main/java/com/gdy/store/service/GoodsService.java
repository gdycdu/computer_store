package com.gdy.store.service;

import com.gdy.store.pojo.Cart;
import com.gdy.store.pojo.Goods;
import com.gdy.store.pojo.GoodsIntact;
import com.gdy.store.pojo.OrderDetail;
import com.gdy.store.tools.PageBean;

import java.math.BigDecimal;
import java.util.List;

public interface GoodsService {
    List<Goods> getAllGoods();

    Goods getGoodsById(int goodsId);

    List<Cart> getCartByUserId(int userId);

    int insertCart(Cart cart);

    int deleteCartByCartId(int CartId);

    Cart getGoodsIdByCartId(int CartId);

    int addCartNums(int cartId);

    int reduceCartNums(int cartId);

    Cart getCartById(int cartId);

    int updataCartNums(int goodId, BigDecimal price, int userId);

    List<Integer> selectCartByGoodIdAndPriceAndUserId(int goodId, BigDecimal price, int userId);

    PageBean<GoodsIntact> getPageBean(int page, int size, String keyWord, int userId);//

    PageBean<GoodsIntact> getPageBean2(int page, int size, String keyWord,String search, int userId);//

    int updateGoods(Goods goods);

    int deleteGoodsByGoodsId(int GoodsId);

    int renewGoods(int goodsId);

    /*发货*/
    int updateGoodsByDelivery(Long id);
    /*确认收货*/
    int getGoodsByIdReceiving(Long id);
    /*完成订单*/
    int getGoodsByOK(Long id);

    OrderDetail selectGoodIdByOrderId(Long OrderId);

    int updataGoodsNumsAndSales(int goodsId,int goodNums);

    Goods selectStoreByGoodsId(int goodsId);

    List<Goods> getGoodsByType(int typeId);

    List<Goods> getGoodsByBrand(int brandId);

    int addGoods(Goods goods);

    Goods getById(int goodsId);

    List<Goods> getGoodsByStoreId(int storeId);

    List<Goods> getGoodsStoreId2(int StoreId);

    int getGoodsCountByStore(int storeId);

    int deleteGoodsByBrand(int brandId);

    int deleteGoodsByStoreId(int storeId);

    int renewGoodsByStoreId(int storeId);

    List<Goods> getGoodsByLikeName(String goodsName);

    int deleteCartByGoods(int goodsId);

    List<Cart> getCartByGoods(int goodsId);
}
