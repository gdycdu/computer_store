package com.gdy.store.service.serviceimpl;

import com.gdy.store.service.CollectService;
import com.gdy.store.mapper.CollectMapper;
import com.gdy.store.pojo.Collect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CollectServiceImpl implements CollectService {
    @Autowired
    private CollectMapper collectMapper;

    @Override
    public int collectGoods(int collectUser, int collectGoods) {
        return collectMapper.collectGoods(collectUser,collectGoods);
    }

    @Override
    public Collect checkCollect(int collectUser, int collectGoods) {
        return collectMapper.checkCollect(collectUser,collectGoods);
    }

    @Override
    public int delCollectById(int collectId) {
        return collectMapper.delCollectById(collectId);
    }

    @Override
    public List<Collect> getAllCollect(int collectUser) {
        return collectMapper.getAllCollect(collectUser);
    }

    @Override
    public int deleteCollectByGoods(int goodsId) {
        return collectMapper.deleteCollectByGoods(goodsId);
    }

    @Override
    public List<Collect> getCollectByGoods(int goodsId) {
        return collectMapper.getCollectByGoods(goodsId);
    }
}
