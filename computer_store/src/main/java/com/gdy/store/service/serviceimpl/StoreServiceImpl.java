package com.gdy.store.service.serviceimpl;

import com.gdy.store.mapper.UserMapper;
import com.gdy.store.pojo.Store;
import com.gdy.store.pojo.StoreUser;
import com.gdy.store.pojo.User;
import com.gdy.store.pojo.UserExample;
import com.gdy.store.service.StoreService;
import com.gdy.store.mapper.StoreMapper;
import com.gdy.store.tools.PageBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StoreServiceImpl implements StoreService {

    @Autowired
    private StoreMapper storeMapper;
    @Autowired
    private UserMapper userMapper;


    @Override
    public int addStoreBecome(Store store) {
        return storeMapper.insertSelective(store);
    }

    @Override
    public PageBean<StoreUser> getPageBean(int page, int size, String keyWord) {
        List<Store> storeList = new ArrayList<>();
        int cnt = storeMapper.getUsersCount();
        int offset = (page <= 0) ? 0 : (page - 1) * size;
        if (keyWord == null) {
            storeList = storeMapper.getAllUsers(offset, size);
        } else {
            UserExample example = new UserExample();
            UserExample.Criteria criteria = example.createCriteria();
            criteria.andUserNameLike("%" + keyWord + "%");
            storeList = storeMapper.selectByExample(example);
        }

        List<User> userList = new ArrayList<>();
        for (int i=0;i<storeList.size();i++){
            User user = userMapper.selectByPrimaryKey(storeList.get(i).getStoreUser());
            userList.add(user);
        }

        List<StoreUser> storeUserList = new ArrayList<>();
        for (int j=0;j<storeList.size();j++){
            StoreUser storeUser = new StoreUser();
            storeUser.setStoreId(storeList.get(j).getStoreId());
            storeUser.setStoreName(storeList.get(j).getStoreName());
            storeUser.setStoreUser(storeList.get(j).getStoreUser());
            storeUser.setStoreDesc(storeList.get(j).getStoreDesc());
            storeUser.setStoreState(storeList.get(j).getStoreState());
            storeUser.setUserName(userList.get(j).getUserName());
            storeUserList.add(storeUser);
        }


        PageBean<StoreUser> pgBean = new PageBean<StoreUser>();
        pgBean.setTotalRecs(cnt); //{ps} 设置总记录数
        pgBean.setList(storeUserList);     //{ps} 设置查出来的结果
        return pgBean;  //返回一个 PageBean
    }


    @Override
    public PageBean<StoreUser> getPageBean2(int page, int size, String keyWord) {
        List<Store> storeList = new ArrayList<>();
        int cnt = storeMapper.getUsersCount2();
        int offset = (page <= 0) ? 0 : (page - 1) * size;
        if (keyWord == null) {
            storeList = storeMapper.getAllUsers2(offset, size);
        } else {
            UserExample example = new UserExample();
            UserExample.Criteria criteria = example.createCriteria();
            criteria.andUserNameLike("%" + keyWord + "%");
            storeList = storeMapper.selectByExample2(example);
        }

        List<User> userList = new ArrayList<>();
        for (int i=0;i<storeList.size();i++){
            User user = userMapper.selectByPrimaryKey(storeList.get(i).getStoreUser());
            userList.add(user);
        }

        List<StoreUser> storeUserList = new ArrayList<>();
        for (int j=0;j<storeList.size();j++){
            StoreUser storeUser = new StoreUser();
            storeUser.setStoreId(storeList.get(j).getStoreId());
            storeUser.setStoreName(storeList.get(j).getStoreName());
            storeUser.setStoreUser(storeList.get(j).getStoreUser());
            storeUser.setStoreDesc(storeList.get(j).getStoreDesc());

            if(storeList.get(j).getStoreState().equals("1")){
                storeUser.setStoreState("正常");
            }
            if(storeList.get(j).getStoreState().equals("2")){
                storeUser.setStoreState("停运");
            }
            if(storeList.get(j).getStoreState().equals("3")){
                storeUser.setStoreState("申请中店铺");
            }
            if(storeList.get(j).getStoreState().equals("4")){
                storeUser.setStoreState("驳回");
            }

            storeUser.setUserName(userList.get(j).getUserName());
            storeUserList.add(storeUser);
        }


        PageBean<StoreUser> pgBean = new PageBean<StoreUser>();
        pgBean.setTotalRecs(cnt); //{ps} 设置总记录数
        pgBean.setList(storeUserList);     //{ps} 设置查出来的结果
        return pgBean;  //返回一个 PageBean
    }



    @Override
    public PageBean<StoreUser> getPageBean3(int page, int size, String keyWord,String search) {
        List<Store> storeList = new ArrayList<>();
        int cnt = storeMapper.getUsersCount3(search);
        int offset = (page <= 0) ? 0 : (page - 1) * size;
        if (keyWord == null) {
            storeList = storeMapper.getAllUsers3(offset, size,search);
        } else {
            UserExample example = new UserExample();
            UserExample.Criteria criteria = example.createCriteria();
            criteria.andUserNameLike("%" + keyWord + "%");
            storeList = storeMapper.selectByExample2(example);
        }

        List<User> userList = new ArrayList<>();
        for (int i=0;i<storeList.size();i++){
            User user = userMapper.selectByPrimaryKey(storeList.get(i).getStoreUser());
            userList.add(user);
        }

        List<StoreUser> storeUserList = new ArrayList<>();
        for (int j=0;j<storeList.size();j++){
            StoreUser storeUser = new StoreUser();
            storeUser.setStoreId(storeList.get(j).getStoreId());
            storeUser.setStoreName(storeList.get(j).getStoreName());
            storeUser.setStoreUser(storeList.get(j).getStoreUser());
            storeUser.setStoreDesc(storeList.get(j).getStoreDesc());

            if(storeList.get(j).getStoreState().equals("1")){
                storeUser.setStoreState("正常");
            }
            if(storeList.get(j).getStoreState().equals("2")){
                storeUser.setStoreState("停运");
            }
            if(storeList.get(j).getStoreState().equals("3")){
                storeUser.setStoreState("申请中店铺");
            }
            if(storeList.get(j).getStoreState().equals("4")){
                storeUser.setStoreState("驳回");
            }

            storeUser.setUserName(userList.get(j).getUserName());
            storeUserList.add(storeUser);
        }


        PageBean<StoreUser> pgBean = new PageBean<StoreUser>();
        pgBean.setTotalRecs(cnt); //{ps} 设置总记录数
        pgBean.setList(storeUserList);     //{ps} 设置查出来的结果
        return pgBean;  //返回一个 PageBean
    }



    @Override
    public int deleteByStoreId(int storeId) {
        return storeMapper.deleteByPrimaryKey(storeId);
    }

    @Override
    public int shenheStore(int storeId, int shenheCode) {
        return storeMapper.shenheStore(storeId,shenheCode);
    }


    @Override
    public int shenqingStore(String storeName, String storeDesc,int userId) {
        Store store = new Store();
        store.setStoreName(storeName);
        store.setStoreDesc(storeDesc);
        store.setStoreState("3");
        store.setStoreUser(userId);

        return storeMapper.insertSelective(store);
    }

    @Override
    public int shenqingStore2(String storeName, String storeDesc, String storeStat, int userId) {
        Store store = new Store();
        store.setStoreName(storeName);
        store.setStoreDesc(storeDesc);
        store.setStoreState(storeStat);
        store.setStoreUser(userId);

        return storeMapper.insertSelective(store);
    }

    @Override
    public Store selectByUserId(int userId) {
        return storeMapper.selectByUserId(userId);
    }

    @Override
    public Store selectByOrderId(int storeId) {
        return storeMapper.selectByPrimaryKey(storeId);
    }

    @Override
    public int updateStoreById(int storeId, String storeName, String storeDesc) {
        return storeMapper.updateStoreById(storeId,storeName,storeDesc);
    }

    @Override
    public int stopOpen(int storeId) {
        return storeMapper.stopOpen(storeId);
    }

    @Override
    public int renewOpen(int storeId) {
        return storeMapper.renewOpen(storeId);
    }

    @Override
    public Store getByGoodsId(int goodsId) {
        return storeMapper.getByGoodsId(goodsId);
    }

    @Override
    public Store getByStoreId(int storeId) {
        return storeMapper.selectByPrimaryKey(storeId);
    }

}
