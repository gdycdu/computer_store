package com.gdy.store.service.serviceimpl;


import com.gdy.store.service.BannerService;
import com.gdy.store.mapper.BannerMapper;
import com.gdy.store.pojo.Banner;
import com.gdy.store.tools.PageBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BannerServiceImpl implements BannerService {
    @Autowired
    private BannerMapper bannerMapper;

    @Override
    public List<Banner> getAllBanner() {
        return bannerMapper.getAllBanner();
    }

    @Override
    public int updateBanner(Banner banner) {
        return bannerMapper.updateBanner(banner);
    }

    @Override
    public int deleteBanner(int bannerId) {
        return bannerMapper.deleteBanner(bannerId);
    }

    @Override
    public int addBanner(Banner banner) {
        return bannerMapper.insertSelective(banner);
    }

    @Override
    public List<Banner> getBannerByLikeName(String bannerName) {

        return bannerMapper.getBannerByLikeName(bannerName);
    }

    @Override
    public List<Banner> getAllBannerByAdmin(int offset, int size) {
        return bannerMapper.getAllBannerByAdmin(offset, size);
    }

    @Override
    public PageBean<Banner> getPageBean(int page, int size, String keyWord) {
        List<Banner> bannerList = new ArrayList<>();
        int cnt = bannerMapper.getBannerCount();
        int offset = (page <= 0) ? 0 : (page - 1) * size;
        if (keyWord == null) {
            bannerList = bannerMapper.getAllBannerByAdmin(offset, size);
        } else {
            bannerList = bannerMapper.getBannerByLikeName(keyWord);
        }
        PageBean<Banner> pgBean = new PageBean<Banner>();
        pgBean.setTotalRecs(cnt); //{ps} 设置总记录数
        pgBean.setList(bannerList);     //{ps} 设置查出来的结果
        return pgBean;  //返回一个 PageBean
    }

    @Override
    public int getBannerCount() {
        return bannerMapper.getBannerCount();
    }

    @Override
    public int selectBanner(int bannerSelect, int bannerId) {
        return bannerMapper.selectBanner(bannerSelect, bannerId);
    }

    @Override
    public int getSelectedBanner() {
        return bannerMapper.getSelectedBanner();
    }
}
