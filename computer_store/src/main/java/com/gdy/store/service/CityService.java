package com.gdy.store.service;


import com.gdy.store.pojo.City;

import java.util.List;

public interface CityService {
    List<City> getCityByProId(int provinceId);

    City getByCityId(int cityId);

    City getByCityName(City city);
}
