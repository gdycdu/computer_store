package com.gdy.store.service;


import com.gdy.store.pojo.Area;

import java.util.List;

public interface AreaService {

    List<Area> getAreaByCityId(int cityId);

    Area getByAreaId(int areaId);

    Area getByAreaName(Area area);
}
