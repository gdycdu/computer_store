package com.gdy.store.service.serviceimpl;

import com.gdy.store.service.OrderDetailService;
import com.gdy.store.mapper.OrderDetailMapper;
import com.gdy.store.pojo.OrderDetail;
import com.gdy.store.pojo.OrderDetailExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderDetailServiceImpl implements OrderDetailService {

    @Autowired
    private OrderDetailMapper detailMapper;

    @Override
    public OrderDetail getOrderDeteilByOrderId(Long OrderId) {
        OrderDetailExample example = new OrderDetailExample();
        OrderDetailExample.Criteria criteria = example.createCriteria();
        criteria.andDetailGoodsEqualTo(OrderId);
        List<OrderDetail> orderDetailList = detailMapper.selectByExample(example);
        return orderDetailList.get(0);
    }

    @Override
    public List<OrderDetail> getByGoodsId(int goodsId) {
        return detailMapper.getByGoodsId(goodsId);
    }

    @Override
    public int deleteByGoods(int goodsId) {
        return detailMapper.deleteByGoods(goodsId);
    }


}
