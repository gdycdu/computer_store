package com.gdy.store.service.serviceimpl;

import com.gdy.store.service.BrandService;
import com.gdy.store.mapper.BrandMapper;
import com.gdy.store.pojo.Brand;
import com.gdy.store.tools.PageBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BrandServiceImpl implements BrandService {
    @Autowired
    BrandMapper brandMapper;

    @Override
    public Brand getByBrandId(int brandId) {
        return brandMapper.selectByPrimaryKey(brandId);
    }

    @Override
    public List<Brand> getAllBrand() {
        return brandMapper.getAllBrand();
    }

    @Override
    public PageBean<Brand> getPageBean(int page, int size, String keyWord) {
        List<Brand> brandList = new ArrayList<>();
        int cnt = brandMapper.getBrandCount();
        int offset = (page <= 0) ? 0 : (page - 1) * size;
        if (keyWord == null) {
            brandList = brandMapper.getAllBrandByAdmin(offset, size);
        } else {
            brandList = brandMapper.getBrandyLikeName(keyWord);
        }
        PageBean<Brand> pgBean = new PageBean<Brand>();
        pgBean.setTotalRecs(cnt); //{ps} 设置总记录数
        pgBean.setList(brandList);     //{ps} 设置查出来的结果
        return pgBean;  //返回一个 PageBean
    }


    @Override
    public int getBrandCount() {
        return brandMapper.getBrandCount();
    }

    @Override
    public List<Brand> getBrandyLikeName(String brandName) {
        return brandMapper.getBrandyLikeName(brandName);
    }

    @Override
    public int updateBrand(int brandId, String brandName) {
        return brandMapper.updateBrand(brandId,brandName);
    }

    @Override
    public int deleteBrand(int brandId) {
        return brandMapper.deleteBrand(brandId);
    }

    @Override
    public int addBrand(String brandName) {
        return brandMapper.addBrand(brandName);
    }
}
