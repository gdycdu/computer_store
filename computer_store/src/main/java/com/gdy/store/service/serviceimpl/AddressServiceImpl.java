package com.gdy.store.service.serviceimpl;

import com.gdy.store.mapper.CityMapper;
import com.gdy.store.mapper.ProvinceMapper;
import com.gdy.store.pojo.Area;
import com.gdy.store.pojo.City;
import com.gdy.store.pojo.Province;
import com.gdy.store.pojo.Street;
import com.gdy.store.service.AreaService;
import com.gdy.store.service.CityService;
import com.gdy.store.service.ProvinceService;
import com.gdy.store.service.StreetService;
import com.gdy.store.mapper.AreaMapper;
import com.gdy.store.mapper.StreetMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressServiceImpl implements ProvinceService, CityService, AreaService, StreetService {
    @Autowired
    private ProvinceMapper provinceMapper;
    @Autowired
    private CityMapper cityMapper;
    @Autowired
    private AreaMapper areaMapper;
    @Autowired
    private StreetMapper streetMapper;
    @Override
    public List<Area> getAreaByCityId(int cityId) {
        return areaMapper.getAreaByCityId(cityId);
    }

    @Override
    public Area getByAreaId(int areaId) {
        return areaMapper.getByAreaId(areaId);
    }

    @Override
    public Area getByAreaName(Area area) {
        return areaMapper.getByAreaName(area);
    }

    @Override
    public List<City> getCityByProId(int provinceId) {
        return cityMapper.getCityByProId(provinceId);
    }

    @Override
    public City getByCityId(int cityId) {
        return cityMapper.getByCityId(cityId);
    }

    @Override
    public City getByCityName(City city) {
        return cityMapper.getByCityName(city);
    }

    @Override
    public List<Province> getAllProv() {
        return provinceMapper.getAllProv();
    }

    @Override
    public Province getByProvId(int provinceId) {
        return provinceMapper.getByProvId(provinceId);
    }

    @Override
    public Province getByProvName(String provinceName) {
        return provinceMapper.getByProvName(provinceName);
    }

    @Override
    public List<Street> getStreetByAreaId(int areaId) {
        return streetMapper.getStreetByAreaId(areaId);
    }

    @Override
    public List<Street> getStreetByCityId(int cityId) {
        return streetMapper.getStreetByCityId(cityId);
    }

    @Override
    public Street getByStreetId(int streetId) {
        return streetMapper.getByStreetId(streetId);
    }

    @Override
    public Street getByStreetName(Street street) {
        return streetMapper.getByStreetName(street);
    }


}
