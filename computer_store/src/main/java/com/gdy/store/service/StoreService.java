package com.gdy.store.service;

import com.gdy.store.pojo.Store;
import com.gdy.store.pojo.StoreUser;
import com.gdy.store.tools.PageBean;

public interface StoreService {
    int addStoreBecome(Store store);

    PageBean<StoreUser> getPageBean(int page, int size, String keyWord);//

    PageBean<StoreUser> getPageBean2(int page, int size, String keyWord);//

    PageBean<StoreUser> getPageBean3(int page, int size, String keyWord,String search);//

    int deleteByStoreId(int storeId);

    int shenheStore(int storeId,int shenheCode);

    int shenqingStore(String storeName,String storeDesc,int userId);

    int shenqingStore2(String storeName,String storeDesc,String storeStat,int userId);

    Store selectByUserId(int userId);

    Store selectByOrderId(int storeId);

    int updateStoreById(int storeId,String storeName,String storeDesc);

    int stopOpen(int storeId);

    int renewOpen(int storeId);

    Store getByGoodsId(int goodsId);

    Store getByStoreId(int storeId);


}
