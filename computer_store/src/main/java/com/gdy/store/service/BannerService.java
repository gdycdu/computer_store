package com.gdy.store.service;


import com.gdy.store.pojo.Banner;
import com.gdy.store.tools.PageBean;

import java.util.List;

public interface BannerService {
    List<Banner> getAllBanner();

    int updateBanner(Banner banner);

    int deleteBanner(int bannerId);

    int addBanner(Banner banner);

    List<Banner> getBannerByLikeName(String bannerName);

    List<Banner> getAllBannerByAdmin(int offset, int size);

    PageBean<Banner> getPageBean(int page, int size, String keyWord);

    int getBannerCount();

    int selectBanner(int bannerSelect, int bannerId);

    int getSelectedBanner();
}
