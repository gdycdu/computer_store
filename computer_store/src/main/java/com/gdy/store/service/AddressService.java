package com.gdy.store.service;


import com.gdy.store.pojo.Address;

import java.util.List;

public interface AddressService {

    int addAddress(Address address);

    List<Address> getAllAddrByUserId(int addrUser);

    List<Address> getAddrByUserId(int addrUser);

    Address getAddrByUserIdByDefault(int userId);

    int delAddrById(int addrId);

    Address getAddrById(int addrId);

    int updateAddress(Address address);

    int updateByPrimaryKeySelective(Address address);

    int setDefaultAddr(int addrId, int userId);

    int setDefaultAddrT(int addrId, int userId);

    Address selectDefaultAddress(int addrId);

    Address selectDefaultAddressByUserId(int userId);

    int updateAddressDefault(int addrId,int userId);


}
