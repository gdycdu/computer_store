package com.gdy.store.service;

import com.gdy.store.pojo.Evaluate;
import com.gdy.store.pojo.Evaluate2;
import com.gdy.store.tools.PageBean;

import java.util.List;

public interface EvaluateService {
    List<Evaluate> getEvaByGoodsId(int evaGoods);

    List<Evaluate> getAllEva();

    int addEvaluate(Evaluate evaluate, String[] imgs);

    PageBean<Evaluate2> getPageBean(int page, int size, String keyWord, int userId);//

    PageBean<Evaluate2> getPageBean2(int page, int size, int search, int userId);//

    int insertEvaluate(Evaluate evaluate);

    List<Evaluate2> getAllEva2(int userId);

    int getCountWhereF(int storeId);

    int getCountWhereO(int storeId);

}
