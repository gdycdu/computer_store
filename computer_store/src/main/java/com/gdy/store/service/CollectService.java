package com.gdy.store.service;

import com.gdy.store.pojo.Collect;

import java.util.List;

public interface CollectService {
    int collectGoods(int collectUser,int collectGoods);

    Collect checkCollect(int collectUser, int collectGoods);

    int delCollectById(int collectId);

    List<Collect> getAllCollect(int collectUser);

    int deleteCollectByGoods(int goodsId);

    List<Collect> getCollectByGoods(int goodsId);
}
