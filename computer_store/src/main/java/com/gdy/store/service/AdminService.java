package com.gdy.store.service;

import com.gdy.store.pojo.Admin;

public interface AdminService {
    Admin adminLogin(Admin admin);
    int updatePwd(Admin admin);
    Admin getAdminById(int adminId);
    int updateAdmin(Admin admin);
}
