package com.gdy.store.service.serviceimpl;

import com.gdy.store.mapper.UserMapper;
import com.gdy.store.service.UserService;
import com.gdy.store.pojo.User;
import com.gdy.store.pojo.UserExample;
import com.gdy.store.tools.PageBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> getAllUsers(int offset, int size) {
        return userMapper.getAllUsers(offset, size);
    }

    @Override
    public PageBean<User> getPageBean(int page, int size, String keyWord) {
        List<User> userList = new ArrayList<>();
        int cnt = userMapper.getUsersCount();
        int offset = (page <= 0) ? 0 : (page - 1) * size;
        if (keyWord == null) {
            userList = userMapper.getAllUsers(offset, size);
        } else {
            UserExample example = new UserExample();
            UserExample.Criteria criteria = example.createCriteria();
            criteria.andUserNameLike("%" + keyWord + "%");
            userList = userMapper.selectByExample(example);
        }
        PageBean<User> pgBean = new PageBean<User>();
        pgBean.setTotalRecs(cnt); //{ps} 设置总记录数
        pgBean.setList(userList);     //{ps} 设置查出来的结果
        return pgBean;  //返回一个 PageBean
    }

    @Override
    public int deleteUser(int userId) {
        return userMapper.deleteUser(userId);
    }

    @Override
    public int changeUserState(int userState, int userId) {
        return userMapper.changeUserState(userState, userId);
    }

    @Override
    public User userLogin(User user) {
        return userMapper.userLogin(user);
    }

    @Override
    public int addUser(User user) {
        return userMapper.insertSelective(user);
    }

    @Override
    public int updateUser(User user) {
        return userMapper.updateByPrimaryKeySelective(user);
    }

    @Override
    public int updatePwd(User user) {
        return userMapper.updatePwd(user);
    }

    @Override
    public User getByName(String userName) {
        return userMapper.getByName(userName);
    }

    @Override
    public User getByPhone(String userPhone) {
        return userMapper.getByPhone(userPhone);
    }

    @Override
    public User getByEmail(String userEmail) {
        return userMapper.getByEmail(userEmail);
    }


    @Override
    public User getById(int id) {
        return userMapper.selectByPrimaryKey(id);
    }

    @Override
    public User getByName2(String userName, int userId) {

        return userMapper.getByName2(userName, userId);
    }

    @Override
    public User getByPhone2(String userName, int userId) {

        return userMapper.getByPhone2(userName, userId);
    }

    @Override
    public User getByEmail2(String userName, int userId) {

        return userMapper.getByEmail2(userName, userId);
    }

    @Override
    public List<User> selectAllUser() {
        return userMapper.selectAllUser();
    }

    @Override
    public User selectUserByUserId(int userId) {
        return userMapper.selectUserByUserId(userId);
    }

    @Override
    public int updataUserRole(String userName) {
        return userMapper.updataUserRole(userName);
    }


}

