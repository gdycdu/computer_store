package com.gdy.store.service.serviceimpl;

import com.gdy.store.service.GoodsTypeService;
import com.gdy.store.mapper.GoodsTypeMapper;
import com.gdy.store.pojo.GoodsType;
import com.gdy.store.pojo.GoodsTypeExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodsTypeServiceImpl implements GoodsTypeService {
    @Autowired
    GoodsTypeMapper goodsTypeMapper;

    @Override
    public List<GoodsType> getByTypeId(int typeId) {
        GoodsTypeExample example = new GoodsTypeExample();
        GoodsTypeExample.Criteria criteria = example.createCriteria();
        criteria.andTypeIdEqualTo(typeId);
        criteria.andTypeStateEqualTo(1);
        return goodsTypeMapper.selectByExample(example);
    }

    @Override
    public GoodsType getTypeByTypeId(int typeId) {
        return goodsTypeMapper.selectTypeByTypeId(typeId);
    }

    @Override
    public List<GoodsType> getAllGoodsType() {
        return goodsTypeMapper.getAllGoodsType();
    }
}
