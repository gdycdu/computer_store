package com.gdy.store.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MyPicConfig implements WebMvcConfigurer {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        String path  = System.getProperty("user.dir")+"\\src\\main\\resources\\static\\img\\header\\";
        registry.addResourceHandler("/header/**").addResourceLocations("file:"+path);
    }
}
